import os, sys, shutil

input_folder = 'src/app/components/patient'
output_folder = 'src/app/components/room'
input_name = 'Room'
output_name = 'MedicalCase'

def main():
    output_folder = edit_name(input_folder, input_name, output_name)
    
    if os.path.isdir(input_folder):
        if not os.path.isdir(output_folder):
            shutil.copytree(input_folder, output_folder)
        edit_folder(output_folder, input_name, output_name)
    elif os.path.isfile(input_folder):
        shutil.copy(input_folder, output_folder)
        edit_file(output_folder, input_name, output_name)


def edit_folder(work_folder, input_name, output_name):
    print("Working on directory (%s)" % work_folder)
    for x in os.listdir(work_folder):
        x = os.path.join(work_folder, x)                
        xnew = edit_name(x, input_name, output_name)
        
        if x == xnew:
            continue

        os.rename(x, xnew) 
        x = xnew

        if os.path.isdir(x):            
            edit_folder(x, input_name, output_name)
        elif os.path.isfile(x):            
            edit_file(x, input_name, output_name)


def edit_name(x, input_name, output_name):
    if input_name in x:
        x = x.replace(input_name, output_name)
    
    if input_name.lower() in x:         
        x = x.replace(input_name.lower(), output_name.lower())
    
    if input_name.upper() in x:
        x = x.replace(input_name.upper(), output_name.upper())

    return x

def edit_file(x, input_name, output_name):
    with open(x, 'r') as file:
        file_text = file.read()

    if input_name in file_text:            
        file_text = file_text.replace(input_name, output_name)
    
    if input_name.lower() in file_text:
        file_text = file_text.replace(input_name.lower(), output_name.lower())
    
    if input_name.upper() in x:
        file_text = file_text.replace(input_name.upper(), output_name.upper())

    with open(x, 'w') as file:
        file.write(file_text)


if __name__ == '__main__':
    main()    