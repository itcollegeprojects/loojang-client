import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { HomeComponent } from './components/pages/home/home.component';
import { RegisterComponent } from './components/account/register/register.component';
import { LoginComponent } from './components/account/login/login.component';
import { LogoutComponent } from './components/account/logout/logout.component';
import { RolelessComponent } from './components/pages/roleless/roleless.component';

import { ViewPatientsComponent } from './components/views/view-patients/view-patients.component';
import { ViewPatientDetailsComponent } from './components/views/view-patient-details/view-patient-details.component';
import { ViewRoomsComponent } from './components/views/view-rooms/view-rooms.component';
import { ViewRoomDetailsComponent } from './components/views/view-room-details/view-room-details.component';
import { ViewMedicalCaseComponent } from './components/views/view-medicalcase/view-medicalcase.component';
import { ViewMedicalCaseDetailsComponent } from './components/views/view-medicalcase-details/view-medicalcase-details.component';

import { PatientsListResolver } from './resolvers/patients-list.resolver';
import { PatientDetailsResolver } from './resolvers/patient-details.resolver';
import { FreeRoomsListResolver } from './resolvers/freerooms-list.resolver';
import { RoomsListResolver } from './resolvers/rooms-list.resolver';
import { RoomDetailsResolver } from './resolvers/room-details.resolver';
import { UsersInDoctorsRoleListResolver } from './resolvers/usersindoctorsrole-list.resolver';
import { UsersInGuardiansRoleListResolver } from './resolvers/usersinguardiansrole.resolver';
import { ProceduresListResolver } from './resolvers/procedure-list.resolver';
import { ProcedureDetailsResolver } from './resolvers/procedure-details.resolver';

import { MedicalCasesListResolver } from './resolvers/medicalcases-list.resolver';
import { MedicalCaseDetailsResolver } from './resolvers/medicalcase-details.resolver';
import { MedicalCasesListByDoctorIdResolver } from './resolvers/medicalcases-list-bydoctorid.resolver';
import { MedicalCasesListByPatientIdResolver } from './resolvers/medicalcases-list-bypatientid.resolver';
import { PatientsRoomResolver } from './resolvers/patientsroom.resolver';
import { PatientsDoctorListResolver } from './resolvers/patientsdoctors-list.resolver';
import { PatientsGuardianListResolver } from './resolvers/patientsguardians-list.resolver';
import { PatientsDoctorByDoctorsAndPatientsIdsResolver } from './resolvers/patientsdoctor-bydoctorsandpatientsids.resolver';
import { FrequencyTypeListResolver } from './resolvers/frequencytype-list.resolver';

import { CanActivateAllUsersGuard , CanActivateChildAllUsersGuard } from './guards/allusers.guard';
import { CanActivateAdminGuard, CanActivateChildAdminGuard } from './guards/admin.guard';
import { CanActivateDoctorGuard, CanActivateChildDoctorGuard } from './guards/doctor.guard';
import { CanActivateGuardianGuard, CanActivateChildGuardianGuard } from './guards/guardian.guard';
import { CanActivateCaretakerGuard, CanActivateChildCaretakerGuard } from './guards/caretaker.guard';
import { ViewProceduresComponent } from './components/views/view-procedures/view-procedures.component';
import { ViewUsersListComponent } from './components/views/view-users-list/view-users-list.component';
import { UsersListResolver } from './resolvers/users-list.resolver';
import { PageNotAuthorizedComponent } from './components/pages/page-not-authorized/page-not-authorized.component';
import { FrequencyType } from './models/frequencytype.model';

const routes: Routes = [
  // General
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },

  // Account
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  // Admin
  {
    path: 'admin',
    canActivateChild: [ CanActivateChildAdminGuard ],
    children: [
      { path: '',
        redirectTo: 'patients',
        pathMatch: 'full'
      },
      {
        path: 'patients',
        component: ViewPatientsComponent,
        resolve: { patients: PatientsListResolver }
      },
      {
        path: 'patients/new',
        component: ViewPatientDetailsComponent,
        resolve: { rooms: FreeRoomsListResolver }
      },
      {
        path: 'patients/:patientId',
        component: ViewPatientDetailsComponent,
        resolve: {
          patient: PatientDetailsResolver,
          patientsRooms: PatientsRoomResolver,
          patientsDoctors: PatientsDoctorListResolver,
          patientsGuardians: PatientsGuardianListResolver,
          rooms: FreeRoomsListResolver,
          doctors: UsersInDoctorsRoleListResolver,
          guardians: UsersInGuardiansRoleListResolver,
          medicalCases: MedicalCasesListByPatientIdResolver
        }
      },
      {
        path: 'rooms',
        component: ViewRoomsComponent,
        resolve: { rooms: RoomsListResolver }
      },
      {
        path: 'rooms/new',
        component: ViewRoomDetailsComponent,
      },
      {
        path: 'rooms/:roomId',
        component: ViewRoomDetailsComponent,
        resolve: { room: RoomDetailsResolver }
      },
      {
        path: 'medicalcases',
        component: ViewMedicalCaseComponent,
        resolve: {
          medicalCases: MedicalCasesListResolver
        }
      },
      {
        path: 'medicalcases/:medicalCaseId',
        component: ViewMedicalCaseDetailsComponent,
        resolve: {
          medicalCase: MedicalCaseDetailsResolver,
          frequencyTypes: FrequencyTypeListResolver
        }
      },
      {
        path: 'procedures',
        component: ViewProceduresComponent,
        resolve: {
          procedures: ProceduresListResolver
        }
      },
      {
        path: 'users',
        component: ViewUsersListComponent,
        resolve: {
          users: UsersListResolver
        }
      }
    ]
  },
  // Guardian
  {
    path: 'guardian',
    canActivateChild: [ CanActivateChildGuardianGuard ],
    children: [
      { path: '',
        redirectTo: 'patients',
        pathMatch: 'full'
      },
      {
        path: 'patients',
        component: ViewPatientsComponent,
        resolve: {
          patients: PatientsListResolver
        }
      },
      {
        path: 'patients/:patientId',
        component: ViewPatientDetailsComponent,
        resolve: {
          patient: PatientDetailsResolver,
          patientsRooms: PatientsRoomResolver,
          patientsDoctors: PatientsDoctorListResolver,
          patientsGuardians: PatientsGuardianListResolver,
          rooms: FreeRoomsListResolver,
          doctors: UsersInDoctorsRoleListResolver,
          guardians: UsersInGuardiansRoleListResolver,
          medicalCases: MedicalCasesListByPatientIdResolver
        }
      },
      {
        path: 'medicalcases',
        component: ViewMedicalCaseComponent,
        resolve: {
          medicalCases: MedicalCasesListByPatientIdResolver
        }
      },
      {
        path: 'medicalcases/:medicalCaseId',
        component: ViewMedicalCaseDetailsComponent,
        resolve: {
          medicalCase: MedicalCaseDetailsResolver
        }
      },
      {
        path: 'procedures',
        component: ViewProceduresComponent,
        resolve: {
          procedures: ProceduresListResolver
        }
      }
    ]
  },
  // Doctor
  {
    path: 'doctor',
    canActivateChild: [ CanActivateChildDoctorGuard ],
    children: [
      { path: '',
        redirectTo: 'patients',
        pathMatch: 'full'
      },
      {
        path: 'patients',
        component: ViewPatientsComponent,
        resolve: {
          patients: PatientsListResolver
        }
      },
      {
        path: 'patients/:patientId',
        component: ViewPatientDetailsComponent,
        resolve: {
          patient: PatientDetailsResolver,
          patientsDoctors: PatientsDoctorListResolver,
          medicalCases: MedicalCasesListByPatientIdResolver
        }
      },
      {
        path: 'medicalcases',
        component: ViewMedicalCaseComponent,
        resolve: {
          medicalCases: MedicalCasesListResolver
        }
      },
      {
        path: 'medicalcases/new',
        component: ViewMedicalCaseDetailsComponent,
        resolve: {
          patientDoctor: PatientsDoctorByDoctorsAndPatientsIdsResolver,
          frequencyTypes: FrequencyTypeListResolver
        }
      },
      {
        path: 'medicalcases/:medicalCaseId',
        component: ViewMedicalCaseDetailsComponent,
        resolve: {
          medicalCase: MedicalCaseDetailsResolver,
          frequencyTypes: FrequencyTypeListResolver
        }
      },
      {
        path: 'procedures',
        component: ViewProceduresComponent,
        resolve: {procedures: ProceduresListResolver}
      }
    ]
  },
  // Caretaker
  {
    path: 'caretaker',
    canActivateChild: [ CanActivateChildCaretakerGuard ],
    children: [
      { path: '',
        redirectTo: 'procedures',
        pathMatch: 'full'
      },
      {
        path: 'procedures',
        component: ViewProceduresComponent,
        resolve: {procedures: ProceduresListResolver}
      }
    ]
  },
  {
    path: 'roleless',
    component: RolelessComponent
  },
  {
    path: '403',
    component: PageNotAuthorizedComponent
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [

    // Guards
    CanActivateAllUsersGuard,
    CanActivateChildAllUsersGuard,
    CanActivateAdminGuard,
    CanActivateChildAdminGuard,
    CanActivateGuardianGuard,
    CanActivateChildGuardianGuard,
    CanActivateDoctorGuard,
    CanActivateChildDoctorGuard,
    CanActivateCaretakerGuard,
    CanActivateChildCaretakerGuard,

    // Resolvers
    PatientsListResolver,
    PatientDetailsResolver,
    PatientsRoomResolver,
    PatientsDoctorListResolver,
    PatientsGuardianListResolver,
    RoomsListResolver,
    RoomDetailsResolver,
    FreeRoomsListResolver,
    MedicalCasesListResolver,
    MedicalCaseDetailsResolver,
    MedicalCasesListByDoctorIdResolver,
    MedicalCasesListByPatientIdResolver,
    PatientsDoctorByDoctorsAndPatientsIdsResolver,
    UsersInDoctorsRoleListResolver,
    UsersInGuardiansRoleListResolver,
    FrequencyTypeListResolver,
    ProceduresListResolver,
    ProcedureDetailsResolver,
    UsersListResolver
  ]
})
export class AppRoutingModule {
}
