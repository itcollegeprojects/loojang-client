import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { PatientDataService } from './services/patient-data.service';
import { PatientsRoomDataService } from './services/patientsroom-data.service';
import { RoomDataService } from './services/room-data.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        PatientDataService,
        PatientsRoomDataService,
        RoomDataService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
