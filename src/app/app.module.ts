import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

// Routing
import { AppRoutingModule } from './app-routing.module';

// General components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { NavbarComponent } from './components/navigation/navbar/navbar.component';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';

// View components
import { ViewPatientsComponent } from './components/views/view-patients/view-patients.component';
import { ViewPatientDetailsComponent } from './components/views/view-patient-details/view-patient-details.component';
import { ViewRoomsComponent } from './components/views/view-rooms/view-rooms.component';
import { ViewRoomDetailsComponent } from './components/views/view-room-details/view-room-details.component';
import { ViewMedicalCaseComponent } from './components/views/view-medicalcase/view-medicalcase.component';
import { ViewMedicalCaseDetailsComponent } from './components/views/view-medicalcase-details/view-medicalcase-details.component';
import { ViewProceduresComponent } from './components/views/view-procedures/view-procedures.component';

// Patient components
import { PatientsListComponent } from './components/entities/patients/patients-list/patients-list.component';
import { PatientsListItemComponent } from './components/entities/patients/patients-list-item/patients-list-item.component';
import { PatientDetailsComponent } from './components/entities/patients/patient-details/patient-details.component';

// Room components
import { RoomsListComponent } from './components/entities/rooms/rooms-list/rooms-list.component';
import { RoomsListItemComponent } from './components/entities/rooms/rooms-list-item/rooms-list-item.component';
import { RoomDetailsComponent } from './components/entities/rooms/room-details/room-details.component';

// MedicalCase components
import { MedicalCasesListComponent } from './components/entities/medicalcases/medicalcases-list/medicalcases-list.component';
import { MedicalCasesListItemComponent } from './components/entities/medicalcases/medicalcases-list-item/medicalcases-list-item.component';
import { MedicalCaseDetailsComponent } from './components/entities/medicalcases/medicalcase-details/medicalcase-details.component';

// PatientRooms components
import { PatientsRoomsListComponent } from './components/entities/patientrooms/patientsrooms-list/patientsrooms-list.component';
import { PatientsRoomsListItemComponent } from './components/entities/patientrooms/patientsrooms-list-item/patientsrooms-list-item.component';

// PatientDoctors components
import { PatientsDoctorsListComponent } from './components/entities/patientsdoctor/patientsdoctors-list/patientsdoctors-list.component';
import { PatientsDoctorsListItemComponent } from './components/entities/patientsdoctor/patientsdoctors-list-item/patientsdoctors-list-item.component';

// PatientsGuardian components
import { PatientsGuardiansListComponent } from './components/entities/patientsguardian/patientsguardians-list/patientsguardians-list.component';
import { PatientsGuardiansListItemComponent } from './components/entities/patientsguardian/patientsguardians-list-item/patientsguardians-list-item.component';


// Procedures components
import { ProceduresListComponent } from './components/entities/procedures/procedures-list/procedures-list.component';
import { ProceduresListItemComponent } from './components/entities/procedures/procedures-list-item/procedures-list-item.component';
import { ProceduresDetailsComponent } from './components/entities/procedures/procedures-details/procedures-details.component';

// Users components
import { UsersListComponent } from './components/entities/users/users-list/users-list.component';
import { UsersListItemComponent } from './components/entities/users/users-list-item/users-list-item.component';

// MedicalCase components

// Services
import { ApiService } from './services/api.service';
import { SessionService } from './services/session.service';
import { UsersDataService } from './services/users-data.service';
import { PatientDataService } from './services/patient-data.service';
import { RoomDataService } from './services/room-data.service';
import { MedicalCaseDataService } from './services/medicalcase-data.service';
import { PatientsRoomDataService } from './services/patientsroom-data.service';
import { PatientsDoctorDataService } from './services/patientsdoctor-data.service';
import { PatientsGuardianDataService } from './services/patientsguardian-data.service';
import { ProcedureDataService } from './services/procedure-data.service';

// Pipes
import { TitleCasePipe } from './pipes/title-case.pipe';
import { SearchPatientsPipe  } from './pipes/search-patients.pipe';
import { SortPatientsPipe } from './pipes/sort-patients.pipe';
import { SearchRoomsPipe  } from './pipes/search-rooms.pipe';
import { SortRoomsPipe } from './pipes/sort-rooms.pipe';
import { SearchPatientsRoomsPipe  } from './pipes/search-patientsrooms.pipe';
import { SortPatientsRoomsPipe } from './pipes/sort-patientsrooms.pipe';
import { SearchMedicalCasesPipe  } from './pipes/search-medicalcases.pipe';
import { SearchPatientsDoctorsPipe  } from './pipes/search-patientsdoctors.pipe';
import { SortPatientsDoctorsPipe } from './pipes/sort-patientsdoctors.pipe';
import { SortMedicalCasesPipe } from './pipes/sort-medicalcases.pipe';
import { NoFutureDatesPipe } from './pipes/nofuturedates.pipe';
import { SearchProceduresPipe  } from './pipes/search-procedures.pipe';
import { SortProceduresPipe } from './pipes/sort-procedures.pipe';

// User
import { RegisterComponent } from './components/account/register/register.component';
import { LoginComponent } from './components/account/login/login.component';
import { LogoutComponent } from './components/account/logout/logout.component';

// Other components
import { TextboxViewEditComponent } from './components/shared/textbox-view-edit/textbox-view-edit.component';
import { TextareaViewEditComponent } from './components/shared/textarea-view-edit/textarea-view-edit.component';
import { DateboxViewEditComponent } from './components/shared/datebox-view-edit/datebox-view-edit.component';

// External modules
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { AuthService } from './services/auth.service';
import { PrescriptionsListComponent } from './components/entities/prescriptions/prescription-list/prescription-list.component';
import { PrescriptionListItemComponent } from './components/entities/prescriptions/prescription-list-item/prescription-list-item.component';
import { SearchPrescriptionsPipe } from './pipes/search-prescptions.pipe';
import { SortPrescriptionsPipe } from './pipes/sort-prescptions.pipe';
import { PrescriptionDetailsComponent } from './components/entities/prescriptions/prescription-details/prescription-details.component';
import { FrequencyTypeDataService } from './services/frequencytype-data.service';
import { PrescriptionDataService } from './services/prescription-data.service';
import { ViewUsersListComponent } from './components/views/view-users-list/view-users-list.component';
import { SortUsersPipe } from './pipes/sort-users.pipe';
import { SearchUsersPipe } from './pipes/search-users.pipe';
import { SortPatientsGuardiansPipe } from './pipes/sort-patientsguardians.pipe';
import { SearchPatientsGuardiansPipe } from './pipes/search-patientsguardians.pipe';
import { PageNotAuthorizedComponent } from './components/pages/page-not-authorized/page-not-authorized.component';
import { RolelessComponent } from './components/pages/roleless/roleless.component';


@NgModule({
  declarations: [
    // General components
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    NavbarComponent,

    // View Components
    ViewPatientsComponent,
    ViewPatientDetailsComponent,
    ViewRoomsComponent,
    ViewRoomDetailsComponent,
    ViewMedicalCaseComponent,
    ViewMedicalCaseDetailsComponent,
    ViewProceduresComponent,

    // Patient
    PatientsListComponent,
    PatientsListItemComponent,
    PatientDetailsComponent,

    // Room
    RoomsListComponent,
    RoomsListItemComponent,
    RoomDetailsComponent,

    // MedicalCase
    MedicalCasesListComponent,
    MedicalCasesListItemComponent,
    MedicalCaseDetailsComponent,

    PatientsRoomsListComponent,
    PatientsRoomsListItemComponent,

    PatientsDoctorsListComponent,
    PatientsDoctorsListItemComponent,

    // Procedure
    ProceduresListComponent,
    ProceduresListItemComponent,
    ProceduresDetailsComponent,

    // Pipes
    TitleCasePipe,
    SearchPatientsPipe,
    SortPatientsPipe,
    SearchRoomsPipe,
    SortRoomsPipe,
    SearchPatientsRoomsPipe,
    SortPatientsRoomsPipe,
    SearchPatientsDoctorsPipe,
    SortPatientsDoctorsPipe,
    SearchMedicalCasesPipe,
    SortMedicalCasesPipe,
    SearchProceduresPipe,
    SortProceduresPipe,

    LoginComponent,
    LogoutComponent,
    RegisterComponent,

    // Other components
    TextboxViewEditComponent,
    TextareaViewEditComponent,
    DateboxViewEditComponent,
    NoFutureDatesPipe,
    PrescriptionsListComponent,
    PrescriptionListItemComponent,
    SearchPrescriptionsPipe,
    SortPrescriptionsPipe,
    PrescriptionDetailsComponent,
    UsersListComponent,
    UsersListItemComponent,
    ViewUsersListComponent,
    SortUsersPipe,
    SearchUsersPipe,
    PatientsGuardiansListComponent,
    PatientsGuardiansListItemComponent,
    SortPatientsGuardiansPipe,
    SearchPatientsGuardiansPipe,
    PageNotAuthorizedComponent,
    RolelessComponent
  ],

  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    DlDateTimePickerDateModule,
    MyDatePickerModule
  ],

  providers:  [
    UsersDataService,
    PatientDataService,
    RoomDataService,
    PatientsRoomDataService,
    PatientsDoctorDataService,
    PatientsGuardianDataService,
    MedicalCaseDataService,
    FrequencyTypeDataService,
    SessionService,
    ApiService,
    AuthService,
    ProcedureDataService,
    PrescriptionDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
