import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public frm: FormGroup;

  public isBusy = false;
  public hasFailed = false;
  public showInputErrors = false;

  constructor(
    private api: ApiService,
    private auth: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.frm = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.auth.isSignedIn()) {
      this.router.navigateByUrl('/' + this.auth.currentRole.toLowerCase());
    }
  }

  public doSignIn() {

    // Make sure form values are valid
    if (this.frm.invalid) {
      this.showInputErrors = true;
      return;
    }

    // Reset status
    this.isBusy = true;
    this.hasFailed = false;

    // Grab values from form
    const email = this.frm.get('username').value;
    const password = this.frm.get('password').value;

    // Submit request to API
    this.api
      .login(email, password)
      .subscribe(
        (response) => {
          this.auth.doSignIn(
            response.token,
            response.roles,
            response.name,
            response.id
          );
        },
        (error) => {
          this.isBusy = false;
          this.hasFailed = true;
        },
        () => {
          this.navigateToRole();
        }
      );
  }

  navigateToRole() {
    if (this.auth.currentRole) {
      this.router.navigate(['/' + this.auth.currentRole.toLowerCase()]);
    } else {
      this.router.navigate(['']);
    }
  }
}

