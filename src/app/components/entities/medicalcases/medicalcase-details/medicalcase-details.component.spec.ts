import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalcaseDetailsComponent } from './medicalcase-details.component';

describe('MedicalcaseDetailsComponent', () => {
  let component: MedicalcaseDetailsComponent;
  let fixture: ComponentFixture<MedicalcaseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalcaseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalcaseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
