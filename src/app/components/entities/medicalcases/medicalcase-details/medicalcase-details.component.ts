import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MedicalCase } from './../../../../models/medicalcase.model';
import { AuthService } from './../../../../services/auth.service';

@Component({
  selector: 'app-medicalcase-details',
  templateUrl: './medicalCase-details.component.html',
  styleUrls: ['./medicalCase-details.component.css']
})
export class MedicalCaseDetailsComponent implements OnInit {

  @Input()
  medicalCase: MedicalCase;

  @Input()
  insertNewMedicalCase: boolean;

  @Input()
  authorized = false;

  @Output()
  medicalCaseChange = new EventEmitter<MedicalCase>();

  @Output()
  medicalCaseDelete = new EventEmitter<MedicalCase>();

  isAdmin: boolean;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.isAdmin = this.auth.currentRole === 'ADMIN';
  }

  saveMedicalCase() {
    this.medicalCaseChange.emit(this.medicalCase);
  }

  deleteMedicalCase() {
    this.medicalCaseDelete.emit(this.medicalCase);
  }
}
