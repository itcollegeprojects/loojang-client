import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalcaseListItemComponent } from './medicalcase-list-item.component';

describe('MedicalcaseListItemComponent', () => {
  let component: MedicalcaseListItemComponent;
  let fixture: ComponentFixture<MedicalcaseListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalcaseListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalcaseListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
