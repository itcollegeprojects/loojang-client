import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MedicalCase } from './../../../../models/medicalcase.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';
import { AuthService } from './../../../../services/auth.service';

@Component({
  selector: '[app-medicalcases-list-item]',
  templateUrl: './medicalcases-list-item.component.html',
  styleUrls: ['./medicalcases-list-item.component.css']
})
export class MedicalCasesListItemComponent implements OnInit {

  @Input() medicalCase: MedicalCase;

  @Output()
  remove: EventEmitter<MedicalCase> = new EventEmitter();

  currentRole: string;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.currentRole = this.auth.currentRole;
  }
}


