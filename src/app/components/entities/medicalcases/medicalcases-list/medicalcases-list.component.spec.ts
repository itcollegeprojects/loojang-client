import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalcaseListComponent } from './medicalcase-list.component';

describe('MedicalcaseListComponent', () => {
  let component: MedicalcaseListComponent;
  let fixture: ComponentFixture<MedicalcaseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalcaseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalcaseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
