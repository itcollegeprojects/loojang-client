import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { MedicalCase } from './../../../../models/medicalcase.model';
import { SearchMedicalCasesPipe } from './../../../../pipes/search-medicalcases.pipe';
import { SortMedicalCasesPipe } from './../../../../pipes/sort-medicalcases.pipe';

@Component({
  selector: 'app-medicalcases-list',
  templateUrl: './medicalcases-list.component.html',
  styleUrls: ['./medicalcases-list.component.css']
})
export class MedicalCasesListComponent implements OnInit {

  sortCriteria = 'createdOn';
  sortOrder = 1;

  @Input()
  searchStringMedicalCase = '';

  @Input()
  medicalCases: MedicalCase[] = [];

  constructor() {
  }

  public ngOnInit() {

  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }
}
