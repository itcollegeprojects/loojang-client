import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsRoom } from './../../../../models/patientsroom.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';

@Component({
  selector: '[app-patientsrooms-list-item]',
  templateUrl: './patientsrooms-list-item.component.html',
  styleUrls: ['./patientsrooms-list-item.component.css']
})
export class PatientsListItemComponent {

  @Input() patient: PatientsRoom;

  @Output()
  remove: EventEmitter<PatientsRoom> = new EventEmitter();

  constructor() { }
}
