import { Component, OnInit, Input } from '@angular/core';
import { PatientsRoom } from '../../../../models/patientsroom.model';
import { SearchPatientsRoomsPipe } from './../../../../pipes/search-patientsrooms.pipe';
import { SortPatientsRoomsPipe } from './../../../../pipes/sort-patientsrooms.pipe';
import { NoFutureDatesPipe } from './../../../../pipes/nofuturedates.pipe';

@Component({
  selector: '[app-patientsrooms-list-item]',
  templateUrl: './patientsrooms-list-item.component.html',
  styleUrls: ['./patientsrooms-list-item.component.css']
})
export class PatientsRoomsListItemComponent implements OnInit {

  sortCriteria: string;

  searchStringPatientsRooms: string;

  @Input() patientsRoom: PatientsRoom;
  @Input() authorized: Boolean;

  constructor() {
  }

  public ngOnInit() {
    this.sortCriteria = 'fromDate';
  }
}