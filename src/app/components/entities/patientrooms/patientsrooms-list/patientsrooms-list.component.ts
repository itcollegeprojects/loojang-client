import { Component, OnInit, Input } from '@angular/core';
import { PatientsRoom } from '../../../../models/patientsroom.model';
import { SearchPatientsRoomsPipe } from './../../../../pipes/search-patientsrooms.pipe'
import { SortPatientsRoomsPipe } from './../../../../pipes/sort-patientsrooms.pipe'

@Component({
  selector: 'app-patientsrooms-list',
  templateUrl: './patientsrooms-list.component.html',
  styleUrls: ['./patientsrooms-list.component.css']
})
export class PatientsRoomsListComponent implements OnInit {

  sortCriteria = 'fromDate';
  sortOrder = 1;

  @Input()
  searchStringPatientsRooms = '';

  @Input()
  patientsRooms: PatientsRoom[] = [];

  constructor() {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }
}
