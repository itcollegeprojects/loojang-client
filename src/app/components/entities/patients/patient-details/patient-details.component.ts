import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Patient } from './../../../../models/patient.model';


@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {

  @Input()
  patient: Patient;

  @Input()
  insertNewPatient: boolean;

  @Input()
  patientIsPresent: boolean;

  @Input()
  authorized = true;

  @Output()
  patientChange = new EventEmitter<Patient>();

  @Output()
  patientDelete = new EventEmitter<Patient>();

  @Output()
  patientLeft = new EventEmitter<Patient>();

  constructor() { }

  ngOnInit() {

  }

  savePatient() {
    this.patientChange.emit(this.patient);
  }

  deletePatient() {
    this.patientDelete.emit(this.patient);
  }

  left() {
    this.patient.leftDate = new Date();
    this.patientLeft.emit(this.patient);
  }

}
