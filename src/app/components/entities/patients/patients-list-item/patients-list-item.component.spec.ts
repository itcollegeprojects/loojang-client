import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsListItemComponent } from './patients-list-item.component';

describe('PatientsListItemComponent', () => {
  let component: PatientsListItemComponent;
  let fixture: ComponentFixture<PatientsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
