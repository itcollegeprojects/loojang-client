import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Patient } from './../../../../models/patient.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';
import { NoFutureDatesPipe } from './../../../../pipes/nofuturedates.pipe';

@Component({
  selector: '[app-patients-list-item]',
  templateUrl: './patients-list-item.component.html',
  styleUrls: ['./patients-list-item.component.css']
})
export class PatientsListItemComponent {

  @Input() patient: Patient;

  @Input() authorized: Boolean;

  @Output()
  remove: EventEmitter<Patient> = new EventEmitter();

  constructor() { }
}
