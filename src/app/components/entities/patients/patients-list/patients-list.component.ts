import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Patient } from '../../../../models/patient.model';
import { SearchPatientsPipe } from './../../../../pipes/search-patients.pipe';
import { SortPatientsPipe } from './../../../../pipes/sort-patients.pipe';

@Component({
  selector: 'app-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.css']
})
export class PatientsListComponent {

  sortCriteria = 'firstName';
  sortOrder = 1;

  @Input()
  searchString = '';

  @Input()
  patients: Patient[] = [];

  constructor() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }
}
