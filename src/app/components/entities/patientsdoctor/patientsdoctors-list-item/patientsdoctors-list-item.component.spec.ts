import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsDoctor } from './../../../../models/patientsdoctor.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';

@Component({
  selector: '[app-patientsdoctors-list-item]',
  templateUrl: './patientsdoctors-list-item.component.html',
  styleUrls: ['./patientsdoctors-list-item.component.css']
})
export class PatientsListItemComponent {

  @Input() patient: PatientsDoctor;

  @Output()
  remove: EventEmitter<PatientsDoctor> = new EventEmitter();

  constructor() { }
}
