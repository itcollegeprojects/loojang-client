import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsDoctor } from '../../../../models/patientsdoctor.model';
import { SearchPatientsDoctorsPipe } from './../../../../pipes/search-patientsdoctors.pipe';
import { SortPatientsDoctorsPipe } from './../../../../pipes/sort-patientsdoctors.pipe';
import { NoFutureDatesPipe } from './../../../../pipes/nofuturedates.pipe';

@Component({
  selector: '[app-patientsdoctors-list-item]',
  templateUrl: './patientsdoctors-list-item.component.html',
  styleUrls: ['./patientsdoctors-list-item.component.css']
})
export class PatientsDoctorsListItemComponent implements OnInit {

  sortCriteria: string;

  assigned = false;

  searchStringPatientsDoctors: string;

  @Output()
  patientsDoctorRemoved = new EventEmitter<PatientsDoctor>();

  @Input()
  patientsDoctor: PatientsDoctor;

  @Input()
  authorized: Boolean;

  constructor() {
  }

  public ngOnInit() {
    this.sortCriteria = 'fromDate';
    if (this.patientsDoctor.toDate > new Date() || !this.patientsDoctor.toDate) {
      this.assigned = true;
    }
  }

  public removeDoctor() {
    this.patientsDoctorRemoved.emit(this.patientsDoctor);
  }
}
