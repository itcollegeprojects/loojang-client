import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsRoomsListComponent } from './patientsrooms-list.component';

describe('PatientsroomsListComponent', () => {
  let component: PatientsRoomsListComponent;
  let fixture: ComponentFixture<PatientsRoomsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsRoomsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsRoomsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
