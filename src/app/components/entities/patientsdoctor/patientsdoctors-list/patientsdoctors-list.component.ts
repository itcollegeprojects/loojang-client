import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsDoctor } from '../../../../models/patientsdoctor.model';
import { SearchPatientsDoctorsPipe } from './../../../../pipes/search-patientsdoctors.pipe';
import { SortPatientsDoctorsPipe } from './../../../../pipes/sort-patientsdoctors.pipe';

@Component({
  selector: 'app-patientsdoctors-list',
  templateUrl: './patientsdoctors-list.component.html',
  styleUrls: ['./patientsdoctors-list.component.css']
})
export class PatientsDoctorsListComponent implements OnInit {

  sortCriteria = 'fromDate';
  sortOrder = 1;

  @Input()
  searchStringPatientsDoctors = '';

  @Input()
  authorized = false;

  @Input()
  patientsDoctors: PatientsDoctor[] = [];

  @Output()
  patientsDoctorRemoved = new EventEmitter<PatientsDoctor>();

  constructor() {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }

  onRemovePatientsDoctor(patientsDoctor: PatientsDoctor) {
    this.patientsDoctorRemoved.emit(patientsDoctor);
  }
}
