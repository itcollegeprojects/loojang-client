import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsguardiansListItemComponent } from './patientsguardians-list-item.component';

describe('PatientsguardiansListItemComponent', () => {
  let component: PatientsguardiansListItemComponent;
  let fixture: ComponentFixture<PatientsguardiansListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsguardiansListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsguardiansListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
