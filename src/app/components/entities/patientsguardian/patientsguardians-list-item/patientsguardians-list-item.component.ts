import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsGuardian } from '../../../../models/patientsguardian.model';
import { SearchPatientsGuardiansPipe } from './../../../../pipes/search-patientsguardians.pipe';
import { SortPatientsGuardiansPipe } from './../../../../pipes/sort-patientsguardians.pipe';
import { NoFutureDatesPipe } from './../../../../pipes/nofuturedates.pipe';

@Component({
  selector: '[app-patientsguardians-list-item]',
  templateUrl: './patientsguardians-list-item.component.html',
  styleUrls: ['./patientsguardians-list-item.component.css']
})
export class PatientsGuardiansListItemComponent implements OnInit {

  sortCriteria: string;

  assigned = false;

  searchStringPatientsGuardians: string;

  @Output()
  patientsGuardianRemoved = new EventEmitter<PatientsGuardian>();

  @Input()
  patientsGuardian: PatientsGuardian;

  @Input()
  authorized: Boolean;

  constructor() {
  }

  public ngOnInit() {
    this.sortCriteria = 'fromDate';
    if (this.patientsGuardian.toDate > new Date() || !this.patientsGuardian.toDate) {
      this.assigned = true;
    }
  }

  public removeGuardian() {
    this.patientsGuardianRemoved.emit(this.patientsGuardian);
  }
}
