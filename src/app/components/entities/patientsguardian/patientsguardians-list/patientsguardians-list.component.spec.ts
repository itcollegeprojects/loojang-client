import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsguardiansListComponent } from './patientsguardians-list.component';

describe('PatientsguardiansListComponent', () => {
  let component: PatientsguardiansListComponent;
  let fixture: ComponentFixture<PatientsguardiansListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsguardiansListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsguardiansListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
