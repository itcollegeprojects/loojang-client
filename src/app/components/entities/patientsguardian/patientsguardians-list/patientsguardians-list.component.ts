import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientsGuardian } from '../../../../models/patientsguardian.model';
import { SearchPatientsGuardiansPipe } from './../../../../pipes/search-patientsguardians.pipe';
import { SortPatientsGuardiansPipe } from './../../../../pipes/sort-patientsguardians.pipe';

@Component({
  selector: 'app-patientsguardians-list',
  templateUrl: './patientsguardians-list.component.html',
  styleUrls: ['./patientsguardians-list.component.css']
})
export class PatientsGuardiansListComponent implements OnInit {

  sortCriteria = 'fromDate';
  sortOrder = 1;

  @Input()
  searchStringPatientsGuardians = '';

  @Input()
  authorized = false;

  @Input()
  patientsGuardians: PatientsGuardian[] = [];

  @Output()
  patientsGuardianRemoved = new EventEmitter<PatientsGuardian>();

  constructor() {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }

  onRemovePatientsGuardian(patientsGuardian: PatientsGuardian) {
    this.patientsGuardianRemoved.emit(patientsGuardian);
  }
}
