import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Prescription } from './../../../../models/prescription.model';
import { FrequencyType } from '../../../../models/frequencytype.model';
import { PrescriptionDataService } from './../../../../services/prescription-data.service';

@Component({
  selector: 'app-prescription-details',
  templateUrl: './prescription-details.component.html',
  styleUrls: ['./prescription-details.component.css']
})
export class PrescriptionDetailsComponent implements OnInit {

  @Input()
  prescription: Prescription;

  @Input()
  insertNewPrescription: boolean;

  @Input()
  authorized = false;

  @Input()
  frequencyTypes: FrequencyType[];

  @Output()
  prescriptionChange = new EventEmitter<Prescription>();

  @Output()
  prescriptionDelete = new EventEmitter<Prescription>();

  @Output()
  prescriptionCancel = new EventEmitter<boolean>();

  frequencyType: FrequencyType;

  constructor(private prescriptionDataService: PrescriptionDataService) { }

  ngOnInit() {
    if (this.frequencyTypes.length > 0) {
      this.frequencyType = this.frequencyTypes[0];
    }
  }

  savePrescription() {
    this.prescription.frequencyTypeId = this.frequencyType.frequencyTypeId;
    console.log('Add');
    console.log(this.prescription);
    this.prescriptionDataService
      .addPrescription(this.prescription)
      .subscribe(p => {
        this.prescription = p;
        console.log('Added');
        console.log(this.prescription);
        this.prescriptionChange.emit(this.prescription);
      });
  }

  deletePrescription() {
    this.prescriptionDelete.emit(this.prescription);
  }

  cancel() {
    this.insertNewPrescription = !this.insertNewPrescription;
    this.prescriptionCancel.emit(false);
  }

  onSelectedFrequencyChange(event) {
    this.frequencyType = this.frequencyTypes[event.target.selectedIndex];
  }
}
