import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionListItemComponent } from './prescription-list-item.component';

describe('PrescriptionListItemComponent', () => {
  let component: PrescriptionListItemComponent;
  let fixture: ComponentFixture<PrescriptionListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescriptionListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
