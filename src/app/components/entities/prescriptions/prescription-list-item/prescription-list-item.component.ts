import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Prescription, PrescriptionDescription } from './../../../../models/prescription.model';
import { FrequencyType } from './../../../../models/frequencytype.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';

@Component({
  selector: '[app-prescription-list-item]',
  templateUrl: './prescription-list-item.component.html',
  styleUrls: ['./prescription-list-item.component.css']
})
export class PrescriptionListItemComponent implements OnInit {

  @Input()
  prescription: PrescriptionDescription ;

  @Input()
  authorized: Boolean;

  @Input()
  frequencyTypes: FrequencyType[];

  @Output()
  remove: EventEmitter<Prescription> = new EventEmitter();

  frequencyTypeName = '';

  constructor() {
  }

  public ngOnInit() {
   const i = this.frequencyTypes.map(ft => ft.frequencyTypeId).indexOf(this.prescription.frequencyTypeId);
    if (i >= 0) {
      this.frequencyTypeName = this.frequencyTypes[i].frequencyTypeName;
    }
  }

  removePrescription() {
    this.remove.emit(this.prescription);
  }
}
