import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Prescription, PrescriptionDescription } from './../../../../models/prescription.model';
import { SearchPrescriptionsPipe } from './../../../../pipes/search-prescptions.pipe';
import { SortPrescriptionsPipe } from './../../../../pipes/sort-prescptions.pipe';
import { FrequencyType } from '../../../../models/frequencytype.model';

@Component({
  selector: 'app-prescription-list',
  templateUrl: './prescription-list.component.html',
  styleUrls: ['./prescription-list.component.css']
})
export class PrescriptionsListComponent implements OnInit {

  sortCriteria = 'startDate';
  sortOrder = 1;

  @Input()
  searchStringPrescriptions = '';

  @Input()
  prescriptions: PrescriptionDescription[] = [];

  @Input()
  frequencyTypes: FrequencyType[];

  @Input()
  authorized: boolean;

  @Output()
  prescriptionsChanged: EventEmitter<PrescriptionDescription[]> = new EventEmitter();

  @Output()
  prescriptionRemoved: EventEmitter<PrescriptionDescription> = new EventEmitter();

  constructor() {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }

  onPrescriptionAdded(newPrescription) {
    this.prescriptions.push(newPrescription);
    this.prescriptionsChanged.emit(this.prescriptions);
  }

  onPrescriptionRemoved(removedPrescription) {
    this.prescriptions = this.prescriptions.filter(p => p !== removedPrescription);
    this.prescriptionRemoved.emit(removedPrescription);
  }
}
