import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceduresDetailsComponent } from './procedures-details.component';

describe('ProceduresDetailsComponent', () => {
  let component: ProceduresDetailsComponent;
  let fixture: ComponentFixture<ProceduresDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceduresDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceduresDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
