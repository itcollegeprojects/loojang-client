import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Procedure } from './../../../../models/procedure.model';

@Component({
  selector: 'app-procedure-details',
  templateUrl: './procedures-details.component.html',
  styleUrls: ['./procedures-details.component.css']
})
export class ProceduresDetailsComponent implements OnInit {

  @Input()
  procedure: Procedure;

  @Input()
  insertNewProcedure: boolean;

  @Input()
  authorized = false;

  @Output()
  procedureChange = new EventEmitter<Procedure>();

  @Output()
  procedureMarkCompleted = new EventEmitter<Procedure>();

  @Output()
  procedureDelete = new EventEmitter<Procedure>();

  constructor() { }

  ngOnInit() {
  }

  saveProcedure() {
    this.procedureChange.emit(this.procedure);
  }

  deleteProcedure() {
    this.procedureDelete.emit(this.procedure);
  }
}
