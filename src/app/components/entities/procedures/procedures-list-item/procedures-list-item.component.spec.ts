import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsListItemComponent } from './procedures-list-item.component';

describe('RoomsListItemComponent', () => {
  let component: RoomsListItemComponent;
  let fixture: ComponentFixture<RoomsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
