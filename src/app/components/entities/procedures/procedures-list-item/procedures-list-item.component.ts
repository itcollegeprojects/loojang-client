import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Procedure } from './../../../../models/procedure.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';
import {AuthService} from '../../../../services/auth.service';

@Component({
  selector: '[app-procedures-list-item]',
  templateUrl: './procedures-list-item.component.html',
  styleUrls: ['./procedures-list-item.component.css']
})
export class ProceduresListItemComponent implements OnInit {

  @Input() procedure: Procedure;

  @Input() authorized: Boolean;

  @Output()
  remove: EventEmitter<Procedure> = new EventEmitter();

  @Output()
  update: EventEmitter<Procedure> = new EventEmitter();

  @Output()
  toggleComplete: EventEmitter<Procedure> = new EventEmitter();

  currentRole: string;

  constructor(
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.currentRole = this.auth.currentRole;
  }

  toggleProcedureComplete(procedure: Procedure) {
    procedure.caretakerId = this.auth.getUserInfo().id;
    this.toggleComplete.emit(procedure);
  }

  deleteProcedure(procedure: Procedure) {
    this.remove.emit(procedure);
  }

  updateProcedure(procedure: Procedure) {
    procedure.caretakerId = this.auth.getUserInfo().id;
    procedure.caretakerName = this.auth.getUserInfo().name;
    procedure.actualDate = new Date();
    this.update.emit(procedure);
  }
}
