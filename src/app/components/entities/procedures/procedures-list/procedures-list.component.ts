import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Procedure } from './../../../../models/procedure.model';
import { ProcedureDataService } from '../../../../services/procedure-data.service';
import { SearchProceduresPipe } from './../../../../pipes/search-procedures.pipe';
import { SortProceduresPipe } from './../../../../pipes/sort-procedures.pipe';
import {SessionService} from '../../../../services/session.service';

@Component({
  selector: 'app-procedures-list',
  templateUrl: './procedures-list.component.html',
  styleUrls: ['./procedures-list.component.css']
})
export class ProceduresListComponent implements OnInit {

  sortCriteria = 'procedureId';

  sortOrder = 1;

  @Output()
  dateFrom: Date;

  @Output()
  dateTo: Date;

  @Input()
  authorized = false;

  @Input()
  searchString = '';

  @Input()
  procedures: Procedure[] = [];

  constructor(
    private procedureDataService: ProcedureDataService
  ) {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }

  onToggleProcedureComplete(procedure) {
    this.procedureDataService
      .toggleProcedureComplete(procedure)
      .subscribe(
        (updatedProcedure) => {
          procedure = updatedProcedure;
        }
      );
  }

  onUpdateProcedure(procedure) {
    this.procedureDataService
      .updateProcedure(procedure)
      .subscribe(
        (updatedProcedure) => {
          const i = this.procedures.map(p => p.procedureId).indexOf(updatedProcedure.prescriptionId);
          this.procedures[i] = updatedProcedure;
        }
      );
  }

  onRemoveProcedure(procedure) {
    this.procedureDataService
      .deleteProcedure(procedure)
      .subscribe((_) => {
        this.procedures = this.procedures.filter(p => p.procedureId !== procedure.procedureId);
    });
  }
}

