import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Room } from './../../../../models/room.model';

@Component({
  selector: 'app-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.css']
})
export class RoomDetailsComponent implements OnInit {

  @Input()
  room: Room;

  @Input()
  insertNewRoom: boolean;

  @Input()
  authorized = false;

  @Output()
  roomChange = new EventEmitter<Room>();

  @Output()
  roomDelete = new EventEmitter<Room>();

  constructor() { }

  ngOnInit() {
  }

  saveRoom() {
    this.roomChange.emit(this.room);
  }

  deleteRoom() {
    this.roomDelete.emit(this.room);
  }
}
