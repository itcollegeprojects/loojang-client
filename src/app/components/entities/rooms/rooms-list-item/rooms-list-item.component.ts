import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Room } from './../../../../models/room.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';

@Component({
  selector: '[app-rooms-list-item]',
  templateUrl: './rooms-list-item.component.html',
  styleUrls: ['./rooms-list-item.component.css']
})
export class RoomsListItemComponent {

  @Input()
  room: Room;

  @Input()
  authorized: Boolean;

  @Output()
  remove: EventEmitter<Room> = new EventEmitter();

  constructor() { }
}
