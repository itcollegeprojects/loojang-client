import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Room } from './../../../../models/room.model';
import { SearchRoomsPipe } from './../../../../pipes/search-rooms.pipe';
import { SortRoomsPipe } from './../../../../pipes/sort-rooms.pipe';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css']
})
export class RoomsListComponent implements OnInit {

  sortCriteria = 'roomCode';
  sortOrder = 1;

  @Input()
  searchString = '';

  @Input()
  rooms: Room[] = [];

  constructor() {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }
}
