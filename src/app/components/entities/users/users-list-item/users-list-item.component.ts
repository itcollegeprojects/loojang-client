import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from './../../../../models/user.model';
import { TitleCasePipe } from './../../../../pipes/title-case.pipe';

@Component({
  selector: '[app-users-list-item]',
  templateUrl: './users-list-item.component.html',
  styleUrls: ['./users-list-item.component.css']
})
export class UsersListItemComponent implements OnInit {


  @Input()
  user: User;

  @Output()
  removed: EventEmitter<User> = new EventEmitter();

  @Output()
  changed: EventEmitter<User> = new EventEmitter();

  role: string;

  availableRoles = ['ADMIN', 'DOCTOR', 'CARETAKER', 'GUARDIAN', 'ROLELESS'];

  constructor() { }

  ngOnInit(): void {
    const roles = this.user.roles.map(r => r.toUpperCase());

    if (roles.indexOf('ADMIN') >= 0) {
      this.role = 'ADMIN';
    } else if (roles.indexOf('DOCTOR') >= 0) {
      this.role = 'DOCTOR';
    } else if (roles.indexOf('CARETAKER') >= 0) {
      this.role = 'CARETAKER';
    } else if (roles.indexOf('GUARDIAN') >= 0) {
      this.role = 'GUARDIAN';
    } else {
      this.role = 'ROLELESS';
    }
  }

  onSelectionChange(newRole: string) {
    this.role = newRole;
    this.user.roles = [newRole.toLowerCase()];
    this.changed.emit(this.user);
  }

  onUserRemove() {
    this.removed.emit(this.user);
  }
}
