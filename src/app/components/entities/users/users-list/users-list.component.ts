import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { User } from './../../../../models/user.model';
import { SearchUsersPipe } from './../../../../pipes/search-users.pipe';
import { SortUsersPipe } from './../../../../pipes/sort-users.pipe';
import { UsersDataService } from './../../../../services/users-data.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  sortCriteria = 'userName';
  sortOrder = 1;

  @Input()
  searchString = '';

  @Input()
  users: User[];

  constructor(
    private usersDataService: UsersDataService
  ) {
  }

  public ngOnInit() {
  }

  adjustSort(newSortCriteria) {
    if (newSortCriteria === this.sortCriteria) {
      this.sortOrder = -1 * this.sortOrder;
    } else {
      this.sortCriteria = newSortCriteria;
      this.sortOrder = 1;
    }
  }

  onUserRemoved(removedUser: User) {
    this.usersDataService.deleteUser(removedUser)
      .subscribe((_) => {
        this.users = this.users.filter(u => u.applicationUserId !== removedUser.applicationUserId);
    });
  }

  onUserChanged(changedUser: User) {
    this.usersDataService.addToRole(changedUser.applicationUserId, changedUser.roles[0])
      .subscribe((_) => {

        this.usersDataService.getAllUsers()
          .subscribe(users => {
            this.users = users;
          });
    });
  }
}
