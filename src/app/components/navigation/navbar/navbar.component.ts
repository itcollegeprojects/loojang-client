import { Component, OnInit, ModuleWithComponentFactories } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  name: string;
  roles: string[];
  router: Router;
  currentRole: string;
  signedIn: boolean;
  index;

  constructor(
    private auth: AuthService,
    router: Router
  ) {
    this.router = router;
  }

  ngOnInit() {
    this.signedIn = this.auth.isSignedIn();

    if (this.signedIn) {
      const user = this.auth.getUserInfo();
      this.name = user['name'];
      this.roles = user['roles'];
      this.currentRole = this.auth.currentRole;
      this.index = this.roles.indexOf(this.auth.currentRole);
    } else {
      this.currentRole = null;
    }
  }

  onRoleChange(event) {
    const newRole = this.roles[event.target.selectedIndex];
    if (this.auth.hasRole(newRole.toUpperCase())) {
      this.auth.currentRole = newRole.toUpperCase();
      this.router.navigateByUrl('/' + newRole.toLowerCase());
    }
  }
}
