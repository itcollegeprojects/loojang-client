import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolelessComponent } from './roleless.component';

describe('RolelessComponent', () => {
  let component: RolelessComponent;
  let fixture: ComponentFixture<RolelessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolelessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolelessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
