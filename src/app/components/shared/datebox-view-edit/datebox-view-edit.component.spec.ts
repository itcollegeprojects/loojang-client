import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateboxViewEditComponent } from './datebox-view-edit.component';

describe('DateboxViewEditComponent', () => {
  let component: DateboxViewEditComponent;
  let fixture: ComponentFixture<DateboxViewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateboxViewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateboxViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
