import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { NoFutureDatesPipe } from './../../../pipes/nofuturedates.pipe';

@Component({
  selector: '[app-datebox-view-edit]',
  templateUrl: './datebox-view-edit.component.html',
  styleUrls: ['./datebox-view-edit.component.css']
})
export class DateboxViewEditComponent implements OnInit {

  @Input()
  value: Date;
  oldValue: Date;

  @Input()
  authorized: false;

  @Output() valueChange = new EventEmitter<Date>();

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy',
    inline: true,
    showTodayBtn: false,
    showSelectorArrow: false,
    showClearDateBtn: true,
    openSelectorOnInputClick: false,
    disableHeaderButtons: false
  }

  viewOrEdit = true;

  constructor() { }

  ngOnInit() {
    if (this.value == null) {
      this.viewOrEdit = false;
    }
    this.oldValue = this.value;
  }

  onDateChanged(event: IMyDateModel) {
    this.value = event.jsdate;
    this.update();
  }

  update() {
    this.oldValue = this.value;
    this.valueChange.emit(this.value);
    this.viewOrEdit = true;
  }

  cancel() {
    this.value = this.oldValue;
    this.viewOrEdit = true;
  }
}
