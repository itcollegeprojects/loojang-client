import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TextareaViewEditComponent } from './textarea-view-edit.component';

describe('TextareaViewEditComponent', () => {
  let component: TextareaViewEditComponent;
  let fixture: ComponentFixture<TextareaViewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextareaViewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextareaViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
