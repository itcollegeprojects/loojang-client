import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextboxViewEditComponent } from './textbox-view-edit.component';

describe('TextboxViewEditComponent', () => {
  let component: TextboxViewEditComponent;
  let fixture: ComponentFixture<TextboxViewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextboxViewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextboxViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
