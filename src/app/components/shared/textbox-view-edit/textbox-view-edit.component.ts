import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-textbox-view-edit]',
  templateUrl: './textbox-view-edit.component.html',
  styleUrls: ['./textbox-view-edit.component.css']
})
export class TextboxViewEditComponent implements OnInit {

  @Input()
  value: String = null;
  oldValue: String = null;

  @Input()
  authorized = false;

  @Output()
  valueChange = new EventEmitter<String>();

  viewOrEdit = true;

  constructor() { }

  ngOnInit() {
    if (this.value == null) {
      this.viewOrEdit = false;
    }
    this.oldValue = this.value;
  }

  onKeydown(event) {
    if (event.key === 'Escape') {
      this.cancel();
    }
  }

  onKeyup(event) {
    if (event.key !== 'Escape') {
      this.valueChange.emit(this.value);
    }
  }

  update() {
    this.oldValue = this.value;
    this.valueChange.emit(this.value);
    this.viewOrEdit = true;
  }

  cancel() {
    this.value = this.oldValue;
    this.viewOrEdit = true;
  }
}
