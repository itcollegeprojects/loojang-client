import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMedicalcaseDetailsComponent } from './view-medicalcase-details.component';

describe('ViewMedicalcaseDetailsComponent', () => {
  let component: ViewMedicalcaseDetailsComponent;
  let fixture: ComponentFixture<ViewMedicalcaseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMedicalcaseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMedicalcaseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
