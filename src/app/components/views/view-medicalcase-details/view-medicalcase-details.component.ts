import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router  } from '@angular/router';
import { PatientsDoctor } from '../../../models/patientsdoctor.model';
import { MedicalCase } from '../../../models/medicalcase.model';
import { FrequencyType } from '../../../models/frequencytype.model';
import { Prescription, PrescriptionDescription } from '../../../models/prescription.model';
import { MedicalCaseDataService } from '../../../services/medicalcase-data.service';
import { AuthService } from '../../../services/auth.service';
import { FrequencyTypeDataService } from '../../../services/frequencytype-data.service';
import { PrescriptionDataService } from '../../../services/prescription-data.service';

@Component({
  selector: 'app-medicalcase',
  templateUrl: './view-medicalcase-details.component.html',
  styleUrls: ['./view-medicalcase-details.component.css'],
  providers: [MedicalCaseDataService, PrescriptionDataService, FrequencyTypeDataService ]
})
export class ViewMedicalCaseDetailsComponent implements OnInit {

  @Input()
  medicalCase: MedicalCase;

  @Input()
  patientsDoctor: PatientsDoctor;

  searchStringPrescriptions = '';

  frequencyTypes: FrequencyType[];

  prescription: Prescription;

  title: string;

  insertNewMedicalCase = false;

  insertNewPrescription = false;

  authorized = false;

  private sub: any;

  constructor(
    private medicalCaseDataService: MedicalCaseDataService,
    private prescriptionDataService: PrescriptionDataService,
    private frequencyTypeDataService: FrequencyTypeDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.initFrequencyTypes();

    this.authorized = this.auth.currentRole === 'DOCTOR' || this.auth.currentRole === 'ADMIN';

    if (this.router.url.startsWith('/doctor/medicalcases/new')) {
      this.initNewMedicalCase();
    } else {
      this.initExistingMedicalCase();
    }
  }

  initFrequencyTypes() {
    this.route.data
    .map((data) => data['frequencyTypes'])
    .subscribe(
      (fts) => {
        this.frequencyTypes = fts;
    });
  }

  initNewMedicalCase() {
    this.insertNewMedicalCase = true;

    this.route.data
    .map((data) => data['patientDoctor'])
    .subscribe(
      (pd) => {
        this.patientsDoctor = pd;
    });

    this.medicalCase = new MedicalCase();
    this.medicalCase.createdOn = new Date();
    this.medicalCase.doctorName = this.auth.getUserInfo().name;
    this.medicalCase.patientId = this.patientsDoctor.patientId;
    this.medicalCase.patientName = this.route.data['name'];
    this.medicalCase.patientsDoctorId = this.patientsDoctor.patientsDoctorId;

    this.sub = this.route.queryParams.subscribe(params => {
      this.medicalCase.patientName = params['patientName'];
    });
  }

  initExistingMedicalCase() {
    this.insertNewMedicalCase = false;

    this.route.data
    .map((data) => data['medicalCase'])
    .subscribe(
      (medicalCase) => {
        this.medicalCase = medicalCase;

    });

    this.title = this.medicalCase.patientName;
  }

  onMedicalCaseChange(medicalCase: MedicalCase) {
    if (this.insertNewMedicalCase) {
      this.addMedicalCase(medicalCase);
    } else {
      this.updateMedicalCase(medicalCase);
    }
  }

  onMedicalCaseDelete(medicalCase: MedicalCase) {
    this.medicalCaseDataService
    .deleteMedicalCaseById(medicalCase.medicalCaseId)
    .subscribe((_) => {
        this.location.back();
      }
    );
  }

  onPrescriptionsChange(prescriptions: PrescriptionDescription[]) {
    this.medicalCase.prescriptions = prescriptions;
  }

  onPrescriptionRemoved(prescriptionRemoved: PrescriptionDescription) {
    this.prescriptionDataService.deletePrescriptionById(prescriptionRemoved.prescriptionId);
  }

  addMedicalCase(medicalCase: MedicalCase) {
    this.medicalCaseDataService
    .addMedicalCase(medicalCase)
    .subscribe((newMedicalCase) => {
      this.medicalCase = newMedicalCase;
      this.medicalCase.prescriptions = [];
      this.insertNewMedicalCase = false;
    });
  }

  updateMedicalCase(medicalCase: MedicalCase) {
    this.medicalCase.prescriptions = [];
    this.medicalCaseDataService
    .updateMedicalCase(medicalCase)
    .subscribe((newMedicalCase) => {
      this.location.back();
    });
  }

  addPrescription() {
    this.prescription = this.createPrescription();
    this.toggleNewPrescription();
  }

  createPrescription(): Prescription {
    const p = new Prescription();
    p.startDate = new Date();
    p.medicalCaseId = this.medicalCase.medicalCaseId;
    return p;
  }

  toggleNewPrescription() {
    this.insertNewPrescription = !this.insertNewPrescription;
  }

  onPrescriptionChange(newPrescription: PrescriptionDescription) {
    this.toggleNewPrescription();

    this.medicalCaseDataService
      .getMedicalCaseById(this.medicalCase.medicalCaseId)
      .subscribe( m => { this.medicalCase = m; });
  }

  onPrescriptionCancel(newInsertPrescription: boolean) {
    this.medicalCase.prescriptions.pop();
    this.insertNewPrescription = newInsertPrescription;
  }
}
