import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMedicalCaseComponent } from './view-medicalcase.component';

describe('ViewMedicalcaseComponent', () => {
  let component: ViewMedicalCaseComponent;
  let fixture: ComponentFixture<ViewMedicalCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMedicalCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMedicalCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
