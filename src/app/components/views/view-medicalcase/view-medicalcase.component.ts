import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MedicalCase } from '../../../models/medicalcase.model';
import { MedicalCaseDataService } from '../../../services/medicalcase-data.service';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-medicalcase',
  templateUrl: './view-medicalcase.component.html',
  styleUrls: ['./view-medicalcase.component.css'],
  providers: [MedicalCaseDataService]
})
export class ViewMedicalCaseComponent implements OnInit {

  @Input()
  searchStringMedicalCase: String = '';

  medicalCases: MedicalCase[] = [];

  authorized = false;

  currentRole = '';

  constructor(
    private medicalCaseDataService: MedicalCaseDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) {
  }

  public ngOnInit(): void {

    this.authorized = this.auth.currentRole === 'DOCTOR' || this.auth.currentRole === 'ADMIN';

    this.currentRole = this.auth.currentRole;

    this.route.data
      .map((data) => data['medicalCases'])
      .subscribe(
        (medicalCases) => {
          if (!!medicalCases) {
            this.medicalCases = medicalCases;
          }
        }
      );
  }
}
