import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router, NavigationExtras  } from '@angular/router';
import { Patient } from '../../../models/patient.model';
import { Room } from '../../../models/room.model';
import { MedicalCase } from '../../../models/medicalcase.model';
import { PatientsRoom } from '../../../models/patientsroom.model';
import { PatientsDoctor } from '../../../models/patientsdoctor.model';
import { PatientDataService } from '../../../services/patient-data.service';
import { PatientsRoomDataService } from '../../../services/patientsroom-data.service';
import { PatientsDoctorDataService } from '../../../services/patientsdoctor-data.service';
import { PatientsGuardianDataService } from '../../../services/patientsguardian-data.service';
import { UsersDataService } from '../../../services/users-data.service';
import { MedicalCaseDataService } from '../../../services/medicalcase-data.service';
import { PatientToRoom } from '../../../models/patienttoroom.model';
import { AuthService } from '../../../services/auth.service';
import { User } from '../../../models/user.model';
import { PatientsGuardian } from '../../../models/patientsguardian.model';

@Component({
  selector: 'app-view-patient-details',
  templateUrl: './view-patient-details.component.html',
  styleUrls: ['./view-patient-details.component.css'],
  providers: [PatientDataService, PatientsRoomDataService, MedicalCaseDataService, PatientsDoctorDataService, UsersDataService]
})
export class ViewPatientDetailsComponent implements OnInit {

  @Input()
  patient: Patient;

  @Input()
  patientsRooms: PatientsRoom[] = [];

  @Input()
  patientsDoctors: PatientsDoctor[] = [];

  @Input()
  patientsGuardians: PatientsGuardian[] = [];

  @Input()
  medicalCases: MedicalCase[] = [];

  @Input()
  rooms: Room[] = [];

  @Input()
  doctors: User[] = [];

  @Input()
  guardians: User[] = [];

  @Input()
  searchStringPatientsRooms: String = '';

  @Input()
  searchStringPatientsDoctors: String = '';

  @Input()
  searchStringPatientsGuardians: String = '';

  @Input()
  searchStringMedicalCase: String = '';

  selectedRoom: Room = new Room();

  selectedDoctor: User = new User();

  selectedGuardian: User = new User();

  patientIsPresent = false;

  title: string;

  insertNewPatient = false;
  authorized = true;
  currentRole: string;

  constructor(
    private patientDataService: PatientDataService,
    private patientsRoomDataService: PatientsRoomDataService,
    private patientsDoctorDataService: PatientsDoctorDataService,
    private patientsGuardianDataService: PatientsGuardianDataService,
    private medicalCaseDataService: MedicalCaseDataService,
    private usersDataService: UsersDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private location: Location
  ) {
  }

  ngOnInit() {

    this.currentRole = this.auth.currentRole;
    this.authorized = this.currentRole === 'ADMIN';

    if (this.router.url.endsWith('/patients/new')) {
      this.rooms = [];
      this.doctors = [];
      this.guardians = [];
      this.medicalCases = [];
      this.initNewPatient();

    } else {
      this.initExistingPatient();
      this.initMedicalCases();
      this.initPatientsDoctors();
      this.initPatientsGuardians();
    }

    this.initRooms();
    this.initDoctors();
    this.initGuardians();


  }

  initNewPatient() {
    this.insertNewPatient = true;
    this.patient = new Patient();
    this.patientsRooms = [];
    this.patientsDoctors = [];
    this.patientsGuardians = [];
    this.medicalCases = [];
  }

  initExistingPatient() {
    this.insertNewPatient = false;

    this.route.data
    .map((data) => data['patient'])
    .subscribe(
      (patient) => {
        this.patient = patient;
        this.patientIsPresent = patient.leftDate === '9999-12-31T23:59:59.9999999';
      }
    );

    this.title = this.patient.getFullName();

    this.route.data
    .map((data) => data['patientsRooms'])
    .subscribe(
      (patientsRooms) => {
        this.patientsRooms = patientsRooms;
      }
    );
  }

  initRooms() {
    this.rooms = [];

    if (!(this.currentRole === 'ADMIN' || this.currentRole === 'GUARDIAN')) {
      return;
    }

    this.route.data
    .map((data) => data['rooms'])
    .subscribe(
      (rooms) => {
        this.rooms = rooms;
      }
    );

    this.updateRooms();
  }

  private updateRooms() {

    if (!this.insertNewPatient && this.patientsRooms.length > 0) {
      this.rooms = this.rooms
        .filter(room => room.roomCode !== this.patientsRooms[this.patientsRooms.length - 1].roomCode);

      if (!this.rooms) {
        this.rooms = [];
      }
    }

    if (!!this.rooms && this.rooms.length > 0) {
      this.selectedRoom = this.rooms[0];
    }
  }

  initDoctors() {
    this.doctors = [];

    if (!(this.currentRole === 'ADMIN' || this.currentRole === 'GUARDIAN')) {
      return;
    }

    this.route.data
    .map((data) => data['doctors'])
    .subscribe(
      (doctors) => {
        this.doctors = doctors;
      }
    );

    this.updateDoctors();
  }

  initGuardians() {
    this.guardians = [];

    if (!(this.currentRole === 'ADMIN' || this.currentRole === 'GUARDIAN')) {  
      return;
    }

    this.route.data
    .map((data) => data['guardians'])
    .subscribe(
      (guardians) => {
        this.guardians = guardians;
      }
    );

    this.updateGuardians();
  }

  private updateDoctors() {
    if (!this.insertNewPatient && this.patientsDoctors.length > 0) {
      const activeDoctors = this.patientsDoctors
          .filter(pd => pd.toDate <= new Date())
          .map(pd => pd.applicationUserId);

      this.doctors = this.doctors
        .filter(doctor => activeDoctors.indexOf(doctor.applicationUserId) < 0);

      if (!this.doctors) {
        this.doctors = [];
      }
    }

    if (!!this.doctors && this.doctors.length > 0) {
      this.selectedDoctor = this.doctors[0];
    }
  }

  private updateGuardians() {
    if (!this.insertNewPatient && this.patientsGuardians.length > 0) {
      const activeGuardians = this.patientsGuardians
          .filter(pd => pd.toDate <= new Date())
          .map(pd => pd.applicationUserId);

      this.guardians = this.guardians
        .filter(guardian => activeGuardians.indexOf(guardian.applicationUserId) < 0);

      if (!this.guardians) {
        this.guardians = [];
      }
    }

    if (!!this.guardians && this.guardians.length > 0) {
      this.selectedGuardian = this.guardians[0];
    }
  }

  initMedicalCases() {
    if (!(this.currentRole === 'ADMIN' || this.currentRole === 'GUARDIAN' || this.currentRole === 'DOCTOR')) {
      return;
    }

    this.route.data
    .map((data) => data['medicalCases'])
    .subscribe(
      (medicalCases) => {
        this.medicalCases = medicalCases;
      }
    );
  }

  initPatientsDoctors() {
    this.route.data
    .map((data) => data['patientsDoctors'])
    .subscribe(
      (patientsDoctors) => {
        this.patientsDoctors = patientsDoctors;
      }
    );
  }

  initPatientsGuardians() {
    this.route.data
    .map((data) => data['patientsGuardians'])
    .subscribe(
      (patientsGuardians) => {
        this.patientsGuardians = patientsGuardians;
      }
    );
  }

  onPatientChange(patient) {
    if (this.insertNewPatient) {
      this.addPatient(patient);
    } else {
      this.updatePatient(patient);
    }
  }

  onPatientDelete(patient) {
    this.deletePatient(patient);
  }

  onPatientLeft(patient) {
    this.patientDataService
    .updatePatient(patient)
    .subscribe(newPatient => {
      this.patient = newPatient;
    });
  }

  onSelectedRoomChange(event) {
    this.selectedRoom = this.rooms[event.target.selectedIndex];
  }

  onSelectedDoctorChange(event) {
    this.selectedDoctor = this.doctors[event.target.selectedIndex];
  }

  onSelectedGuardianChange(event) {
    this.selectedGuardian = this.guardians[event.target.selectedIndex];
  }

  addPatient(patient: Patient) {
    this.patientDataService
    .addPatient(patient)
    .subscribe(newPatient => {
      this.insertNewPatient = false;
      this.patientIsPresent = true;
      this.patient = newPatient;
      console.log('/' + this.currentRole.toLowerCase() + '/patients/' + this.patient.patientId);
      this.router.navigateByUrl('/' + this.currentRole.toLowerCase() + '/patients/' + this.patient.patientId);
    });
  }

  updatePatient(patient: Patient) {
    this.patientDataService
    .updatePatient(patient)
    .subscribe((_) => {
      this.location.back();
    });
  }

  deletePatient(patient: Patient) {
    this.patientDataService
    .deletePatientById(patient.patientId)
    .subscribe(
      (_) => {
        this.location.back();
      }
    );
  }

  moveToRoom() {
    this.patientsRoomDataService
    .addPatientToRoom({
      roomId: this.selectedRoom.roomId,
      patientId: this.patient.patientId,
      fromDate: new Date(),
      toDate: null
    })
    .subscribe(newPatientToRoom => {
      this.patientsRoomDataService
        .getPatientsRoomsByPatientId(this.patient.patientId)
        .subscribe(rooms => {
          this.patientsRooms = rooms;
          this.updateRooms();
        });
    });
  }

  assignToDoctor() {
    this.patientsDoctorDataService
      .addPatientToDoctor({
        applicationUserId: this.selectedDoctor.applicationUserId,
        patientId: this.patient.patientId,
        fromDate: new Date(),
        toDate: null
      })
      .subscribe(newPatientToDoctor => {
        this.patientsDoctorDataService
          .getPatientsDoctorsByPatientId(this.patient.patientId)
          .subscribe(() => {

            this.patientsDoctorDataService
              .getPatientsDoctorsByPatientId(this.patient.patientId)
              .subscribe(newPatientsDoctors => {
                this.patientsDoctors = newPatientsDoctors;

                this.usersDataService
                .getUsersInRole('DOCTOR')
                .subscribe(newDoctors => {
                  this.doctors = newDoctors;
                  this.updateDoctors();
                });
              });

          });
      });
  }

  assignToGuardian() {
    this.patientsGuardianDataService
      .addPatientToGuardian({
        applicationUserId: this.selectedGuardian.applicationUserId,
        patientId: this.patient.patientId,
        fromDate: new Date(),
        toDate: null
      })
      .subscribe(newPatientToGuardian => {
        this.patientsGuardianDataService
          .getPatientsGuardiansByPatientId(this.patient.patientId)
          .subscribe(() => {

            this.patientsGuardianDataService
              .getPatientsGuardiansByPatientId(this.patient.patientId)
              .subscribe(newPatientsGuardians => {
                this.patientsGuardians = newPatientsGuardians;

                this.usersDataService
                .getUsersInRole('GUARDIAN')
                .subscribe(newGuardians => {
                  this.guardians = newGuardians;
                  this.updateGuardians();
                });
              });

          });
      });
  }

  onRemovePatientsDoctor(patientsDoctor) {
    if (patientsDoctor.toDate > new Date() || !patientsDoctor.toDate) {
      // Unassign
      patientsDoctor.toDate = new Date();
      this.patientsDoctorDataService
      .updatePatientFromDoctor(patientsDoctor)
      .subscribe(() => {

        this.patientsDoctorDataService
        .getPatientsDoctorsByPatientId(this.patient.patientId)
        .subscribe(newPatientsDoctors => {
          this.patientsDoctors = newPatientsDoctors;

          this.usersDataService
          .getUsersInRole('DOCTOR')
          .subscribe(newDoctors => {
            this.doctors = newDoctors;
            this.updateDoctors();
          });

        });
      });

    } else {
      // Remove
      this.patientsDoctorDataService
      .removePatientFromDoctor(patientsDoctor)
      .subscribe(() => {

        this.patientsDoctorDataService
        .getPatientsDoctorsByPatientId(this.patient.patientId)
        .subscribe(newPatientsDoctors => {
          this.patientsDoctors = newPatientsDoctors;

          this.usersDataService
          .getUsersInRole('DOCTOR')
          .subscribe(newDoctors => {
            this.doctors = newDoctors;
            this.updateDoctors();
          });

        });
      });
    }
  }

  onRemovePatientsGuardian(patientsGuardian) {
    if (patientsGuardian.toDate > new Date() || !patientsGuardian.toDate) {
      // Unassign
      patientsGuardian.toDate = new Date();
      this.patientsGuardianDataService
      .updatePatientFromGuardian(patientsGuardian)
      .subscribe(() => {

        this.patientsGuardianDataService
        .getPatientsGuardiansByPatientId(this.patient.patientId)
        .subscribe(newPatientsGuardians => {
          this.patientsGuardians = newPatientsGuardians;

          this.usersDataService
          .getUsersInRole('GUARDIAN')
          .subscribe(newGuardians => {
            this.guardians = newGuardians;
            this.updateGuardians();
          });

        });
      });

    } else {
      // Remove
      this.patientsGuardianDataService
      .removePatientFromGuardian(patientsGuardian)
      .subscribe(() => {

        this.patientsGuardianDataService
        .getPatientsGuardiansByPatientId(this.patient.patientId)
        .subscribe(newPatientsGuardians => {
          this.patientsGuardians = newPatientsGuardians;

          this.usersDataService
          .getUsersInRole('GUARDIAN')
          .subscribe(newGuardians => {
            this.guardians = newGuardians;
            this.updateGuardians();
          });

        });
      });
    }
  }

  createMedicalCase() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        'patientId': this.patient.patientId,
        'patientName': this.patient.firstName + ' ' + this.patient.lastName
      }
     };
    this.router.navigate(['/doctor/medicalcases/new'], navigationExtras);
  }
}
