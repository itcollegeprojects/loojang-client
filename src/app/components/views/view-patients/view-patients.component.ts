import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Patient } from '../../../models/patient.model';
import { PatientsRoom } from '../../../models/patientsroom.model';
import { PatientDataService } from '../../../services/patient-data.service';
import { PatientsRoomDataService } from '../../../services/patientsroom-data.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-patients',
  templateUrl: './view-patients.component.html',
  styleUrls: ['./view-patients.component.css'],
  providers: [PatientDataService]
})
export class ViewPatientsComponent implements OnInit {

  @Input()
  searchString = '';

  patients: Patient[] = [];

  authorized = false;

  constructor(
    private patientDataService: PatientDataService,
    private route: ActivatedRoute,
    private auth: AuthService
  ) {
  }

  public ngOnInit() {
    this.authorized = this.auth.currentRole === 'ADMIN';
    this.route.data
      .map((data) => data['patients'])
      .subscribe(
        (patients) => {
          this.patients = patients;
        }
      );
  }
}