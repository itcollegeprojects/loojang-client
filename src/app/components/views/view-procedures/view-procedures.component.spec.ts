import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProceduresComponent } from './view-procedures.component';

describe('ProceduresComponent', () => {
  let component: ViewProceduresComponent;
  let fixture: ComponentFixture<ViewProceduresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProceduresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProceduresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
