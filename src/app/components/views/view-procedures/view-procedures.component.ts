import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Procedure } from '../../../models/procedure.model';
import { AuthService } from '../../../services/auth.service';
import {ProcedureDataService} from '../../../services/procedure-data.service';


@Component({
  selector: 'app-procedures',
  templateUrl: './view-procedures.component.html',
  styleUrls: ['./view-procedures.component.css'],
  providers: [ProcedureDataService]
})
export class ViewProceduresComponent implements OnInit {

  @Input()
  searchString: String = '';

  procedures: Procedure[] = [];

  authorized = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private procedureDataService: ProcedureDataService
  ) {
  }

  public ngOnInit(): void {

    this.authorized = this.auth.currentRole !== 'GUARDIAN';

    this.route.data
      .map((data) => data['procedures'])
      .subscribe(
        (procedures) => {
          this.procedures = procedures;
        }
      );
  }
}
