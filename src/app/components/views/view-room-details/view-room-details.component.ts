import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';
import { Location } from '@angular/common';
import { Room } from '../../../models/room.model';
import { RoomDataService } from '../../../services/room-data.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-room',
  templateUrl: './view-room-details.component.html',
  styleUrls: ['./view-room-details.component.css'],
  providers: [RoomDataService]
})
export class ViewRoomDetailsComponent implements OnInit {

  @Input()
  room: Room;

  title: string;

  insertNewRoom = false;

  authorized = false;

  constructor(
    private roomDataService: RoomDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private location: Location
  ) {
  }

  ngOnInit() {

    this.authorized = this.auth.currentRole === 'ADMIN';

    if (this.router.url === '/rooms/new') {
      this.initNewRoom();

    } else {
      this.initExistingRoom();
    }
  }

  initNewRoom() {
    this.insertNewRoom = true;
    this.room = new Room();
  }

  initExistingRoom() {
    this.insertNewRoom = false;
    this.route.data
    .map((data) => data['room'])
    .subscribe(
      (room) => {
        this.room = room;
    });
    this.title = this.room.roomCode;
  }

  onRoomChange(room) {
    if (this.insertNewRoom) {
      this.addRoom(room);
    } else {
      this.updateRoom(room);
    }
  }

  onRoomDelete(room) {
    this.roomDataService
    .deleteRoomById(room.roomId)
    .subscribe(
      (_) => {
        this.location.back();
      }
    );
  }

  addRoom(room: Room) {
    this.roomDataService
    .addRoom(room)
    .subscribe((newRoom) => {
      this.room = newRoom;
      this.insertNewRoom = false;
    });
  }

  updateRoom(room: Room) {
    this.roomDataService
    .updateRoom(room)
    .subscribe((newRoom) => {
      this.location.back();
    });
  }
}
