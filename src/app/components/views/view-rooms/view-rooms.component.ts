import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Room } from '../../../models/room.model';
import { RoomDataService } from '../../../services/room-data.service';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-rooms',
  templateUrl: './view-rooms.component.html',
  styleUrls: ['./view-rooms.component.css'],
  providers: [RoomDataService]
})
export class ViewRoomsComponent implements OnInit {

  @Input()
  searchString: String = '';

  rooms: Room[] = [];

  authorized = false;

  constructor(
    private roomDataService: RoomDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) {
  }

  public ngOnInit(): void {

    this.authorized = this.auth.currentRole === 'ADMIN';

    this.route.data
      .map((data) => data['rooms'])
      .subscribe(
        (rooms) => {
          this.rooms = rooms;
        }
      );
  }
}