import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../models/user.model';
import { UsersDataService } from '../../../services/users-data.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-view-users-list',
  templateUrl: './view-users-list.component.html',
  styleUrls: ['./view-users-list.component.css']
})
export class ViewUsersListComponent implements OnInit {

  @Input()
  searchString: String = '';

  users: User[] = [];

  authorized = false;

  constructor(
    private userDataService: UsersDataService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) {
  }

  ngOnInit() {
    this.authorized = this.auth.currentRole === 'ADMIN';

    this.route.data
      .map((data) => data['users'])
      .subscribe(
        (users) => {
          console.log('rsolve');
          console.log(users);
          this.users = users;
        }
      );
  }
}
