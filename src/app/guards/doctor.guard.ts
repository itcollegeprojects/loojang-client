import { Injectable } from '@angular/core';
import { AuthService } from './../services/auth.service';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CanActivateDoctorGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('doctors here: ' + this.auth.isSignedIn() + ' ' + this.auth.hasRole('DOCTOR'));
    if (!this.auth.isSignedIn() || !this.auth.hasRole('DOCTOR')) {
      this.auth.doSignOut();
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}

@Injectable()
export class CanActivateChildDoctorGuard implements CanActivateChild {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  public canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('doctors here: ' + this.auth.isSignedIn() + ' ' + this.auth.hasRole('DOCTOR'));
    if (!this.auth.isSignedIn() || !this.auth.hasRole('DOCTOR')) {
      this.auth.doSignOut();
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
