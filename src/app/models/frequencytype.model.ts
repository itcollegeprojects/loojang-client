export class FrequencyType {
    frequencyTypeId: number;
    frequencyTypeName: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

