import { Prescription, PrescriptionDescription } from './prescription.model';

export class MedicalCase {
    medicalCaseId: number;
    patientId: number;
    patientName: string;
    patientsDoctorId: number;
    doctorId: string;
    doctorName: string;
    createdOn: Date;
    comment: string;
    prescriptions: PrescriptionDescription[];

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
