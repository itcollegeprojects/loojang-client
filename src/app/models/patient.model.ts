export class Patient {
    patientId: number;
    firstName: string;
    lastName: string;
    personalCode: string;
    room: string;
    birthDay: Date;
    joinedDate: Date;
    leftDate: Date;
    comment: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

    getFullName() {
        return this.firstName + ' ' + this.lastName;
    }
}
