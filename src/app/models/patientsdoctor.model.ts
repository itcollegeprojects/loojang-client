export class PatientsDoctor {
    patientsDoctorId: number;
    applicationUserId: string;
    firstName: string;
    lastName: string;
    fromDate: Date;
    toDate: Date;
    patientId: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
