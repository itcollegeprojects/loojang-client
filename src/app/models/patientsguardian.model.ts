export class PatientsGuardian {
    patientsGuardianId: number;
    applicationUserId: string;
    firstName: string;
    lastName: string;
    fromDate: Date;
    toDate: Date;
    patientId: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
