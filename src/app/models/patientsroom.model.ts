export class PatientsRoom {
    roomId: number;
    roomCode: string;
    fromDate: Date;
    toDate: Date;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
