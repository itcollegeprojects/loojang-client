export class PatientToDoctor {
    applicationUserId: string;
    patientId: number;
    fromDate: Date;
    toDate: Date;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
