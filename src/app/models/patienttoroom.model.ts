export class PatientToRoom {
    roomId: number;
    patientId: number;
    fromDate: Date;
    toDate: Date;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
