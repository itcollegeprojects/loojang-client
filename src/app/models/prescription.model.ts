export class Prescription {
    prescriptionId: number;
    description: string;
    startDate: Date;
    endDate: Date;
    comment: string;
    frequencyTypeId: number;
    medicalCaseId: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class PrescriptionDescription {
    prescriptionId: number;
    description: string;
    startDate: Date;
    endDate: Date;
    comment: string;
    frequencyTypeId: number;
    frequencyTypeName: string;
    medicalCaseId: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
