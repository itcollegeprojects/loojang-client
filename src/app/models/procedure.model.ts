import {Observable} from 'rxjs/Observable';

export class Procedure {
  procedureId: number;
  scheduledDate: Date;
  actualDate: Date;
  prescriptionId: number;
  prescriptionDescription: string;
  comment: string;
  caretakerId: string;
  caretakerName: string;
  patientId: number;
  patientName: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

  public isCompleted(): boolean {
    return this.actualDate != null || this.actualDate <= new Date();
  }
}
