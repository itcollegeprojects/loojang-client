export class RegisterUser {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
