export class Room {
    roomId: number;
    roomCode: string;
    capacity: number;
    free: number;
    comment: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
      }
}
