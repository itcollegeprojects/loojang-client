export class User {
    applicationUserId: string;
    firstName: string;
    lastName: string;
    roles: string[];
    email: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
