import { Constants } from './../util/constants';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'noFutureDates'
})
export class NoFutureDatesPipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value === '9999-12-31T23:59:59.9999999') {
      return null;
    } else {
      return super.transform(value, Constants.DATE_FMT);
    }
  }
}