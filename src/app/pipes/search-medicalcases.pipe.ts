import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchMedicalCasesPipe',
  pure: false
})
export class SearchMedicalCasesPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if (!data) { return []; }
    searchTerm = String(searchTerm).toUpperCase();
    return data.filter(item => {
      return String(item.comment + ' ' + item.patientName + ' ' + item.doctorName).toUpperCase().indexOf(searchTerm) !== -1;
    });
  }
}
