import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPatientsPipe',
  pure: false
})
export class SearchPatientsPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if(!data) {
      return [];
    }

    searchTerm = String(searchTerm).toUpperCase();

    return data.filter(item => {
        return String(item.firstName + ' ' + item.lastName + ' ' + item.personalCode).toUpperCase().indexOf(searchTerm) !== -1
      }
    );
  }
}
