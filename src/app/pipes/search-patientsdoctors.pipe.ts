import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPatientsDoctorsPipe',
  pure: false
})
export class SearchPatientsDoctorsPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if (!data) { return []; }
    searchTerm = String(searchTerm).toUpperCase();
    return data.filter(item => {
      return String(item.firstName + ' ' + item.lastName).toUpperCase().indexOf(searchTerm) !== -1
    });
  }
}
