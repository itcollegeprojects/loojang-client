import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPatientsGuardiansPipe',
  pure: false
})
export class SearchPatientsGuardiansPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if (!data) { return []; }
    searchTerm = String(searchTerm).toUpperCase();
    return data.filter(item => {
      return String(item.firstName + ' ' + item.lastName).toUpperCase().indexOf(searchTerm) !== -1;
    });
  }
}
