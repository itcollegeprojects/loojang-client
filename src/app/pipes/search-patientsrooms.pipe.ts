import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPatientsRoomsPipe',
  pure: false
})
export class SearchPatientsRoomsPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if(!data) { return []; }
    searchTerm = String(searchTerm).toUpperCase();
    return data.filter(item => {
      return String(item.roomCode).toUpperCase().indexOf(searchTerm) !== -1
    });
  }
}
