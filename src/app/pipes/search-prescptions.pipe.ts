import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPrescriptionsPipe'
})
export class SearchPrescriptionsPipe implements PipeTransform {

  transform(data: any[], searchTerm: string): any[] {
    if (!data) { return []; }

    searchTerm = String(searchTerm).toUpperCase();

    return data.filter(item => {
      return String(item.description + ' ' + item.comment).toUpperCase().indexOf(searchTerm) !== -1;
    });
  }

}
