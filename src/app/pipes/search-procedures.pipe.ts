import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchProceduresPipe',
  pure: false
})
export class SearchProceduresPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {
    if(!data) { return []; }
    searchTerm = String(searchTerm).toUpperCase();
    return data.filter(item => {
      return String(item.procedureId).toUpperCase().indexOf(searchTerm) !== -1
    });
  }
}
