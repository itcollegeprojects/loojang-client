import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchRoomsPipe',
  pure: false
})
export class SearchRoomsPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {

    if (!data) { return []; }

    searchTerm = String(searchTerm).toUpperCase();

    return data.filter(item => {
      return String(item.roomCode).toUpperCase().indexOf(searchTerm) !== -1
    });
  }
}
