import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchUsersPipe',
  pure: false
})
export class SearchUsersPipe implements PipeTransform {
  transform(data: any[], searchTerm: string): any[] {

    if (!data) { return []; }

    searchTerm = String(searchTerm).toUpperCase();

    return data.filter(item => {
      return String(item.firstName + ' ' + item.LastName + ' ' + item.email).toUpperCase().indexOf(searchTerm) !== -1
    });
  }
}
