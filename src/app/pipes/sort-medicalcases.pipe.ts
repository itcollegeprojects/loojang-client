import { Pipe, PipeTransform } from '@angular/core';
import { MedicalCase } from './../models/medicalcase.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortMedicalCasesPipe'
})
export class SortMedicalCasesPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<MedicalCase>, args: string, ascending: number): Array<MedicalCase> {

    this.ascending = ascending;

    switch (args) {
      case 'comment':
        array.sort((a: MedicalCase, b: MedicalCase) => this.compareStrings(a.comment, b.comment));
        break;

      case 'patientName':
        array.sort((a: MedicalCase, b: MedicalCase) => this.compareStrings(a.patientName, b.patientName));
        break;

      case 'doctorName':
        array.sort((a: MedicalCase, b: MedicalCase) => this.compareStrings(a.doctorName, b.doctorName));
        break;

      case 'createdOn':
        array.sort((a: MedicalCase, b: MedicalCase) => this.compareDates(a.createdOn, b.createdOn));
        break;

      default:
        array.sort((a: MedicalCase, b: MedicalCase) => this.compareDates(a.createdOn, b.createdOn));
        break;
      }

    return array;

  }
}
