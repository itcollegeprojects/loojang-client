import { Pipe, PipeTransform } from '@angular/core';
import { Patient } from './../models/patient.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortPatientsPipe'
})
export class SortPatientsPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<Patient>, args: string, ascending: number): Array<Patient> {

    this.ascending = ascending;

    switch (args) {
      case 'fullName':
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.getFullName(), b.getFullName()));
        break;

      case 'firstName':
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.firstName, b.firstName));
        break;

      case 'lastName':
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.lastName, b.lastName));
        break;

      case 'personalCode':
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.personalCode, b.personalCode));
        break;

      case 'room':
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.room, b.room));
        break;

      case 'birthDay':
        array.sort((a: Patient, b: Patient) => this.compareDates(a.birthDay, b.birthDay));
        break;

      case 'dateJoined':
        array.sort((a: Patient, b: Patient) => this.compareDates(a.joinedDate, b.joinedDate));
        break;

      case 'dateLeft':
        array.sort((a: Patient, b: Patient) => this.compareDates(a.leftDate, b.leftDate));
        break;

      default:
        array.sort((a: Patient, b: Patient) => this.compareStrings(a.getFullName(), b.getFullName()));
        break;
      }

    return array;

  }
}