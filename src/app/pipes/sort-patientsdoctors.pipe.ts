import { Pipe, PipeTransform } from '@angular/core';
import { PatientsDoctor } from './../models/patientsdoctor.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortPatientsDoctorsPipe'
})
export class SortPatientsDoctorsPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<PatientsDoctor>, args: string, ascending: number): Array<PatientsDoctor> {

    this.ascending = ascending;

    switch (args) {
      case 'firstName':
        array.sort((a: PatientsDoctor, b: PatientsDoctor) => this.compareStrings(a.firstName, b.firstName));
        break;

      case 'lastName':
        array.sort((a: PatientsDoctor, b: PatientsDoctor) => this.compareStrings(a.lastName, b.lastName));
        break;

      case 'fromDate':
        array.sort((a: PatientsDoctor, b: PatientsDoctor) => this.compareDates(a.fromDate, b.toDate));
        break;

      case 'toDate':
        array.sort((a: PatientsDoctor, b: PatientsDoctor) => this.compareDates(a.toDate, b.toDate));
        break;

      default:
        array.sort((a: PatientsDoctor, b: PatientsDoctor) => this.compareStrings(a.lastName, b.lastName));
        break;
      }

    return array;

  }
}
