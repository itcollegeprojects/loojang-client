import { Pipe, PipeTransform } from '@angular/core';
import { PatientsGuardian } from './../models/patientsguardian.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortPatientsGuardiansPipe'
})
export class SortPatientsGuardiansPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<PatientsGuardian>, args: string, ascending: number): Array<PatientsGuardian> {

    this.ascending = ascending;

    switch (args) {
      case 'firstName':
        array.sort((a: PatientsGuardian, b: PatientsGuardian) => this.compareStrings(a.firstName, b.firstName));
        break;

      case 'lastName':
        array.sort((a: PatientsGuardian, b: PatientsGuardian) => this.compareStrings(a.lastName, b.lastName));
        break;

      case 'fromDate':
        array.sort((a: PatientsGuardian, b: PatientsGuardian) => this.compareDates(a.fromDate, b.toDate));
        break;

      case 'toDate':
        array.sort((a: PatientsGuardian, b: PatientsGuardian) => this.compareDates(a.toDate, b.toDate));
        break;

      default:
        array.sort((a: PatientsGuardian, b: PatientsGuardian) => this.compareStrings(a.lastName, b.lastName));
        break;
      }

    return array;

  }
}
