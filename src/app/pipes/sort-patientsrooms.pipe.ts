import { Pipe, PipeTransform } from '@angular/core';
import { PatientsRoom } from './../models/patientsroom.model';
import { TitleCasePipe } from './../pipes/title-case.pipe'

@Pipe({
  name: 'sortPatientsRoomsPipe'
})
export class SortPatientsRoomsPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<PatientsRoom>, args: string, ascending: number): Array<PatientsRoom> {

    this.ascending = ascending;

    switch (args) {
      case 'roomCode':
        array.sort((a: PatientsRoom, b: PatientsRoom) => this.compareStrings(a.roomCode, b.roomCode));
        break;

      case 'fromDate':
        array.sort((a: PatientsRoom, b: PatientsRoom) => this.compareDates(a.fromDate, b.toDate));
        break;

      case 'toDate':
        array.sort((a: PatientsRoom, b: PatientsRoom) => this.compareDates(a.toDate, b.toDate));
        break;

      default:
        array.sort((a: PatientsRoom, b: PatientsRoom) => this.compareStrings(a.roomCode, b.roomCode));
        break;
      }

    return array;

  }
}
