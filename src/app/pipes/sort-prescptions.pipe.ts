import { Pipe, PipeTransform } from '@angular/core';
import { Prescription } from './../models/prescription.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortPrescriptionsPipe'
})
export class SortPrescriptionsPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<Prescription>, args: string, ascending: number): Array<Prescription> {

    this.ascending = ascending;

    switch (args) {
      case 'description':
        array.sort((a: Prescription, b: Prescription) => this.compareStrings(a.description, b.description));
        break;

      case 'comment':
        array.sort((a: Prescription, b: Prescription) => this.compareStrings(a.comment, b.comment));
        break;

      case 'startDate':
        array.sort((a: Prescription, b: Prescription) => this.compareDates(a.startDate, b.startDate));
        break;

      case 'endDate':
        array.sort((a: Prescription, b: Prescription) => this.compareDates(a.endDate, b.endDate));
        break;

      default:
      array.sort((a: Prescription, b: Prescription) => this.compareDates(a.startDate, b.startDate));
        break;
      }

    return array;

  }
}
