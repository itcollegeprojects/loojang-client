import { Pipe, PipeTransform } from '@angular/core';
import { Procedure } from './../models/procedure.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortProceduresPipe'
})
export class SortProceduresPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<Procedure>, args: string, ascending: number): Array<Procedure> {

    this.ascending = ascending;

    switch (args) {
      case 'patientName':
        array.sort((a: Procedure, b: Procedure) => this.compareStrings(a.patientName, b.patientName));
        break;

      case 'caretakerName':
        array.sort((a: Procedure, b: Procedure) => this.compareStrings(a.caretakerName, b.caretakerName));
        break;

      case 'prescriptionDescription':
        array.sort((a: Procedure, b: Procedure) => this.compareStrings(a.prescriptionDescription, b.prescriptionDescription));
        break;

      case 'comment':
        array.sort((a: Procedure, b: Procedure) => this.compareStrings(a.comment, b.comment));
        break;

      case 'procedureId':
        array.sort((a: Procedure, b: Procedure) => this.compareNumbers(a.procedureId, b.procedureId));
        break;

      case 'scheduledDate':
        array.sort((a: Procedure, b: Procedure) => this.compareDates(a.scheduledDate, b.scheduledDate));
        break;

      case 'actualDate':
        array.sort((a: Procedure, b: Procedure) => this.compareDates(a.actualDate, b.actualDate));
        break;

/*
      default:
        array.sort((a: Procedure, b: Procedure) => this.compareNumbers(a.procedureId, b.procedureId));
        break;
*/
      }


    return array;

  }
}
