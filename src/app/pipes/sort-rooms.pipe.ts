import { Pipe, PipeTransform } from '@angular/core';
import { Room } from './../models/room.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortRoomsPipe'
})
export class SortRoomsPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<Room>, args: string, ascending: number): Array<Room> {

    this.ascending = ascending;

    switch (args) {
      case 'roomCode':
        array.sort((a: Room, b: Room) => this.compareStrings(a.roomCode, b.roomCode));
        break;

      case 'capacity':
        array.sort((a: Room, b: Room) => this.compareNumbers(a.capacity, b.capacity));
        break;

      case 'free':
        array.sort((a: Room, b: Room) => this.compareNumbers(a.free, b.free));
        break;

      default:
        array.sort((a: Room, b: Room) => this.compareStrings(a.roomCode, b.roomCode));
        break;
      }

    return array;

  }
}
