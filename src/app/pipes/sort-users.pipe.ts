import { Pipe, PipeTransform } from '@angular/core';
import { User } from './../models/user.model';
import { TitleCasePipe } from './../pipes/title-case.pipe';

@Pipe({
  name: 'sortUsersPipe'
})
export class SortUsersPipe implements PipeTransform {

  ascending = 1;

  casePipe: TitleCasePipe = new TitleCasePipe();

  compareStrings(a: string, b: string) {
    a = this.casePipe.transform(a);
    b = this.casePipe.transform(b);
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareDates(a: Date, b: Date) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  compareNumbers(a: number, b: number) {
    if (a < b) {
      return -1 * this.ascending;
    } else if (a > b) {
      return 1 * this.ascending;
    } else {
      return 0;
    }
  }

  transform(array: Array<User>, args: string, ascending: number): Array<User> {

    this.ascending = ascending;

    switch (args) {
      case 'email':
        array.sort((a: User, b: User) => this.compareStrings(a.email, b.email));
        break;

      case 'firstName':
        array.sort((a: User, b: User) => this.compareStrings(a.firstName, b.firstName));
        break;

      case 'lastName':
        array.sort((a: User, b: User) => this.compareStrings(a.lastName, b.lastName));
        break;

      default:
        array.sort((a: User, b: User) => this.compareStrings(a.email, b.email));
        break;
      }

    return array;

  }
}
