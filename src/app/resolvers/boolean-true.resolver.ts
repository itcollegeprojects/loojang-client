import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Room } from './../models/room.model';
import { RoomDataService } from './../services/room-data.service';

@Injectable()
export class BooleanTrueResolver implements Resolve<Observable<Boolean>> {

  constructor( ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Boolean> {
    return Observable.create(true).delay(10);
  }
}