import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FrequencyType } from './../models/frequencyType.model';
import { FrequencyTypeDataService } from './../services/frequencytype-data.service';

@Injectable()
export class FrequencyTypeListResolver implements Resolve<Observable<FrequencyType[]>> {

  constructor( private frequencyTypeDataService: FrequencyTypeDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<FrequencyType[]> {
    return this.frequencyTypeDataService.getAllFrequencyTypes();
  }
}
