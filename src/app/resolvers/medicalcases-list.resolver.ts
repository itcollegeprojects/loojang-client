import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MedicalCase } from './../models/medicalcase.model';
import { MedicalCaseDataService } from './../services/medicalcase-data.service';

@Injectable()
export class MedicalCasesListResolver implements Resolve<Observable<MedicalCase[]>> {

  constructor( private medicalCaseDataService: MedicalCaseDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<MedicalCase[]> {
    return this.medicalCaseDataService.getAllMedicalCases();
  }
}