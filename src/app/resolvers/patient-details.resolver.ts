import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Patient } from './../models/patient.model';
import { PatientDataService } from './../services/patient-data.service';

@Injectable()
export class PatientDetailsResolver implements Resolve<Observable<Patient>> {

  constructor( private patientDataService: PatientDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Patient> {
    return this.patientDataService.getPatientById(route.params.patientId);
  }
}