import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PatientsDoctor } from './../models/patientsdoctor.model';
import { PatientsDoctorDataService } from './../services/patientsdoctor-data.service';
import { AuthService } from './../services/auth.service';

@Injectable()
export class PatientsDoctorByDoctorsAndPatientsIdsResolver implements Resolve<Observable<PatientsDoctor>> {

  constructor(
    private patientsDoctorDataService: PatientsDoctorDataService,
    private auth: AuthService
   ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PatientsDoctor> {
    return this.patientsDoctorDataService
          .getPatientsDoctorsByDoctorsAndPatientsIds(
            this.auth.getUserInfo().id,
            route.queryParams.patientId
          );
  }
}
