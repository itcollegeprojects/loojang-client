import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PatientsDoctor } from './../models/patientsdoctor.model';
import { PatientsDoctorDataService } from './../services/patientsdoctor-data.service';

@Injectable()
export class PatientsDoctorListResolver implements Resolve<Observable<PatientsDoctor[]>> {

  constructor( private patientsDoctorDataService: PatientsDoctorDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PatientsDoctor[]> {
    return this.patientsDoctorDataService.getPatientsDoctorsByPatientId(route.params.patientId);
  }
}
