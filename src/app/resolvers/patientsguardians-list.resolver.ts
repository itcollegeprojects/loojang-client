import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PatientsGuardian } from './../models/patientsguardian.model';
import { PatientsGuardianDataService } from './../services/patientsguardian-data.service';

@Injectable()
export class PatientsGuardianListResolver implements Resolve<Observable<PatientsGuardian[]>> {

  constructor( private patientsGuardianDataService: PatientsGuardianDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PatientsGuardian[]> {
    return this.patientsGuardianDataService.getPatientsGuardiansByPatientId(route.params.patientId);
  }
}
