import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PatientsRoom } from './../models/patientsroom.model';
import { PatientsRoomDataService } from './../services/patientsroom-data.service';

@Injectable()
export class PatientsRoomResolver implements Resolve<Observable<PatientsRoom[]>> {

  constructor( private patientsRoomDataService: PatientsRoomDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PatientsRoom[]> {
    return this.patientsRoomDataService.getPatientsRoomsByPatientId(route.params.patientId);
  }
}