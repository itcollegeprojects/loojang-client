import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Prescription } from './../models/prescription.model';
import { PrescriptionDataService } from './../services/prescription-data.service';

@Injectable()
export class PrescriptionsListByMedicalCaseResolver implements Resolve<Observable<Partial<Prescription>[]>> {

    constructor( private prescriptionDataService: PrescriptionDataService ) {
    }

    public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<Prescription[]> {
        return this.prescriptionDataService.getPrescriptionsByMedicalCaseId(route.params.medicalCaseId);
    }
}
