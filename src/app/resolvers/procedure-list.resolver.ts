import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Procedure } from './../models/procedure.model';
import { ProcedureDataService } from './../services/procedure-data.service';

@Injectable()
export class ProceduresListResolver implements Resolve<Observable<Procedure[]>> {

  constructor( private procedureDataService: ProcedureDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Procedure[]> {
    return this.procedureDataService.getActiveProcedures();
  }
}
