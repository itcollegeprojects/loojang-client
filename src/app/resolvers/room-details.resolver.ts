import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Room } from './../models/room.model';
import { RoomDataService } from './../services/room-data.service';

@Injectable()
export class RoomDetailsResolver implements Resolve<Observable<Room>> {

  constructor( private roomDataService: RoomDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Room> {
    return this.roomDataService.getRoomById(route.params.roomId);
  }
}