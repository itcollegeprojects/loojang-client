import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from './../models/user.model';
import { UsersDataService } from './../services/users-data.service';

@Injectable()
export class UsersListResolver implements Resolve<Observable<User[]>> {

  constructor( private usersDataService: UsersDataService ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<User[]> {
    return this.usersDataService.getAllUsers();
  }
}
