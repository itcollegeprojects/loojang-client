import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { RegisterUser } from '../models/register-user.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AccountService {

  constructor(
    private api: ApiService
  ) {
  }

  addUser(user: RegisterUser): Observable<RegisterUser> {
    return this.api.addUser(user);
  }

}
