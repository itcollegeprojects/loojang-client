import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, Request, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from './session.service';

import { Patient } from '../models/patient.model';
import { Room } from '../models/room.model';
import { PatientsRoom } from '../models/patientsroom.model';
import { PatientToRoom } from '../models/patienttoroom.model';
import { PatientsDoctor } from '../models/patientsdoctor.model';
import { PatientToDoctor } from '../models/patienttodoctor.model';
import { PatientsGuardian } from '../models/patientsguardian.model';
import { PatientToGuardian } from '../models/patienttoguardian.model';
import { MedicalCase } from '../models/medicalcase.model';
import { Prescription, PrescriptionDescription } from '../models/prescription.model';
import { User } from '../models/user.model';
import { RegisterUser } from '../models/register-user.model';
import { ActivatedRoute, Router  } from '@angular/router';
import { FrequencyType } from '../models/frequencytype.model';
import { Procedure } from '../models/procedure.model';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

  constructor(
    private http: Http,
    private router: Router,
    private session: SessionService
  ) {
  }

  // API: POST /patientrooms/
  public addPatientToRoom(patientToRoom: PatientToRoom): Observable<PatientToRoom> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/patientrooms/', patientToRoom, options)
      .map(response => {
        return new PatientsRoom(response.json());
      })
      .catch(this.handleError);
  }

  // API: GET /api/patients
  public getAllPatients(): Observable<Patient[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients', options)
      .map(response => {
        const patients = response.json();
        return patients.map((patient) => new Patient(patient));
      })
      .catch(this.handleError);
  }

  // API: GET /api/patients/role/{role}
  public getPatientsByRole(role: string): Observable<Patient[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients/role/' + role.toLowerCase(), options)
      .map(response => {
        const patients = response.json();
        return patients.map((patient) => new Patient(patient));
      })
      .catch(this.handleError);
  }

  // API: GET /patients/:patientId
  public getPatientById(patientId: number): Observable<Patient> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients/' + patientId, options)
      .map(response => {
        return new Patient(response.json());
      })
      .catch(
        response => {
          if (response.status === 404) {
            this.router.navigate(['404']);
          } else {
            return this.handleError(response);
          }
        });
  }

  // API: POST /patients/
  public addPatient(patient: Patient): Observable<Patient> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/patients', patient, options)
      .map(response => {
        return new Patient(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /patients/:patientId
  public updatePatient(patient: Patient): Observable<Patient> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/patients/' + patient.patientId, patient, options)
      .map(response => {
        return new Patient(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /patients/:patientId
  public deletePatientById(patientId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/patients/' + patientId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /patients/:patientId/rooms
  public getPatientsRoomsByPatientId(patientId: number): Observable<PatientsRoom[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients/' + patientId + '/rooms', options)
      .map(response => {
        const patientsrooms = response.json();
        return patientsrooms.map((patientsRoom) => new PatientsRoom(patientsRoom));
      })
      .catch(this.handleError);
  }

  // API: GET /api/rooms
  public getAllRooms(): Observable<Room[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/rooms', options)
      .map(response => {
        const rooms = response.json();
        return rooms.map((room) => new Room(room));
      })
      .catch(this.handleError);
  }

  // API: GET /api/rooms
  public getFreeRooms(): Observable<Room[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/rooms/free', options)
      .map(response => {
        const rooms = response.json();
        return rooms.map((room) => new Room(room));
      })
      .catch(this.handleError);
  }

  // API: GET /rooms/:roomId
  public getRoomById(roomId: number): Observable<Room> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/rooms/' + roomId, options)
      .map(response => {
        return new Room(response.json());
      })
      .catch(this.handleError);
  }

  // API: POST /rooms/
  public addRoom(room: Room): Observable<Room> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/rooms/', room, options)
      .map(response => {
        return new Room(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /rooms/:roomId
  public updateRoom(room: Room): Observable<Room> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/rooms/' + room.roomId, room, options)
      .map(response => {
        return new Room(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /rooms/:roomId
  public deleteRoomById(roomId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/rooms/' + roomId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /api/procedures
  public getAllProcedures(): Observable<Procedure[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/procedures', options)
      .map(response => {
        const procedures = response.json();
        return procedures.map((procedure) => new Procedure(procedure));
      })
      .catch(this.handleError);
  }

  // API: GET /procedures/:procedureId
  public getProcedureById(procedureId: number): Observable<Procedure> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/procedures/' + procedureId, options)
      .map(response => {
        return new Procedure(response.json());
      })
      .catch(this.handleError);
  }

  // API: POST /procedures/
  public addProcedure(procedure: Procedure): Observable<Procedure> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/procedures/', procedure, options)
      .map(response => {
        return new Procedure(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /procedures/:procedureId
  public updateProcedure(procedure: Procedure): Observable<Procedure> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/procedures/' + procedure.procedureId, procedure, options)
      .map(response => {
        return new Procedure(response.json());
      })
      .catch(this.handleError);
  }

  toggleProcedureComplete(procedure: Procedure): Observable<Procedure> {

    if (!procedure.isCompleted()) {
      procedure.actualDate = new Date();
    } else {
      procedure.actualDate = null;
    }

    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/procedures/' + procedure.procedureId, procedure, options)
      .map(response => {
        return new Procedure(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /procedures/:procedureId
  public deleteProcedureById(procedureId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/procedures/' + procedureId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /procedures/startdate=2018-01-01&enddate=2018-02-01
  public getProceduresBetweenDates(dateFrom: Date, dateTo: Date): Observable<Procedure[]>  {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/procedures/startdate=' + dateFrom + '&enddate=' + dateTo, options)
      .map(response => {
        const procedures = response.json();
        return procedures.map((procedure) => new Procedure(procedure));
      })
      .catch(this.handleError);
  }

  // API: GET /procedures/?onlyactive=t
  public getActiveProcedures(): Observable<Procedure[]>  {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/procedures/?onlyactive=t', options)
      .map(response => {
        const procedures = response.json();
        return procedures.map((procedure) => new Procedure(procedure));
      })
      .catch(this.handleError);
  }

  // API: GET /api/medicalcases
  public getAllMedicalCases(): Observable<MedicalCase[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/medicalcases', options)
      .map(response => {
        const medicalCases = response.json();
        return medicalCases.map((medicalCase) => new MedicalCase(medicalCase));
      })
      .catch(this.handleError);
  }

  // API: GET /api/medicalcases/doctor/{doctorId}
  public getMedicalCasesByDoctorId(doctorId: string): Observable<MedicalCase[]> {
    const options = this.getRequestOptions();
    console.log(API_URL + '/api/medicalcases/doctor/' + doctorId);
    return this.http
      .get(API_URL + '/api/medicalcases/doctor/' + doctorId, options)
      .map(response => {
        const medicalCases = response.json();
        return medicalCases.map((medicalCase) => new MedicalCase(medicalCase));
      })
      .catch(this.handleError);
  }

  // API: GET /api/medicalcases/patient/{patientId}
  public getMedicalCasesByPatientId(patientId: number): Observable<MedicalCase[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/medicalcases/patient/' + patientId, options)
      .map(response => {
        const medicalCases = response.json();
        return medicalCases.map((medicalCase) => new MedicalCase(medicalCase));
      })
      .catch(this.handleError);
  }

  // API: GET /medicalcases/:medicalCaseId
  public getMedicalCaseById(medicalCaseId: number): Observable<MedicalCase> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/medicalcases/' + medicalCaseId, options)
      .map(response => {
        return new MedicalCase(response.json());
      })
      .catch(this.handleError);
  }

  // API: POST /medicalcases/
  public addMedicalCase(medicalCase: Partial<MedicalCase>): Observable<MedicalCase> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/medicalcases/', medicalCase, options)
      .map(response => {
        return new MedicalCase(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /medicalcases/:medicalCaseId
  public updateMedicalCase(medicalCase: MedicalCase): Observable<MedicalCase> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/medicalcases/' + medicalCase.medicalCaseId, medicalCase, options)
      .map(response => {
        return new MedicalCase(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /medicalCases/:medicalCaseId
  public deleteMedicalCaseById(medicalCaseId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/medicalcases/' + medicalCaseId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /api/prescriptions
  public getAllPrescriptions(): Observable<Prescription[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/prescriptions', options)
      .map(response => {
        const prescriptions = response.json();
        return prescriptions.map((prescription) => new Prescription(prescription));
      })
      .catch(this.handleError);
  }

  // API: GET /api/medicalcases/{medicalCaseId}/prescriptions
  public getPrescriptionsByMedicalCaseId(medicalCaseId: number): Observable<PrescriptionDescription[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/medicalcases/' + medicalCaseId + '/prescriptions', options)
      .map(response => {
        const prescriptions = response.json();
        return prescriptions.map((prescription) => new PrescriptionDescription(prescription));
      })
      .catch(this.handleError);
  }

  // API: GET /prescriptions/:prescriptionId
  public getPrescriptionById(prescriptionId: number): Observable<PrescriptionDescription> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/prescriptions/' + prescriptionId, options)
      .map(response => {
        return new PrescriptionDescription(response.json());
      })
      .catch(this.handleError);
  }

  // API: POST /prescriptions/
  public addPrescription(prescription: Prescription): Observable<Prescription> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/prescriptions/withprocedures', prescription, options)
      .map(response => {
        return new Prescription(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /prescriptions/:prescriptionId
  public updatePrescription(prescription: Prescription): Observable<Prescription> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/prescriptions/' + prescription.prescriptionId, prescription, options)
      .map(response => {
        return new Prescription(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /prescriptions/:prescriptionId
  public deletePrescriptionById(prescriptionId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/prescriptions/' + prescriptionId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /patients/:patientId/doctors
  public getPatientsDoctorsByPatientId(patientId: number): Observable<PatientsDoctor[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients/' + patientId + '/doctors', options)
      .map(response => {
        const patientsdoctor = response.json();
        return patientsdoctor.map((patientsDoctor) => new PatientsDoctor(patientsDoctor));
      })
      .catch(this.handleError);
  }

  // API: POST /patientsdoctor/
  public addPatientsDoctor(patientToDoctor: PatientToDoctor): Observable<PatientToDoctor> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/patientsdoctors/', patientToDoctor, options)
      .map(response => {
        return new PatientToDoctor(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /patientsdoctors/:patientsdoctorId
  public updatePatientsDoctor(patientsDoctor: PatientsDoctor): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/patientsdoctors/' + patientsDoctor.patientsDoctorId, patientsDoctor, options)
      .map(response => {
        return new PatientsDoctor(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /patientsdoctors/:patientsdoctorId
  public deletePatientsDoctorById(patientsDoctorId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/patientsdoctors/' + patientsDoctorId, options)
      .map(response => null)
      .catch(this.handleError);
  }

  // API: GET /patients/:patientId/guardians
  public getPatientsGuardiansByPatientId(patientId: number): Observable<PatientsGuardian[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/patients/' + patientId + '/guardians', options)
      .map(response => {
        const patientsguardian = response.json();
        return patientsguardian.map((patientsGuardian) => new PatientsGuardian(patientsGuardian));
      })
      .catch(this.handleError);
  }

  // API: POST /patientsguardian/
  public addPatientsGuardian(patientToGuardian: PatientToGuardian): Observable<PatientToGuardian> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/patientsguardians/', patientToGuardian, options)
      .map(response => {
        return new PatientToGuardian(response.json());
      })
      .catch(this.handleError);
  }

  // API: PATCH /patientsguardians/:patientsguardianId
  public updatePatientsGuardian(patientsGuardian: PatientsGuardian): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/patientsguardians/' + patientsGuardian.patientsGuardianId, patientsGuardian, options)
      .map(response => {
        return new PatientsGuardian(response.json());
      })
      .catch(this.handleError);
  }

  // API: DELETE /patientsguardians/:patientsguardianId
  public deletePatientsGuardianById(patientsGuardianId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/patientsguardians/' + patientsGuardianId, options)
      .map(response => null)
      .catch(this.handleError);
  }
  // API: GET /api/users/
  public getAllUsers(): Observable<User[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/users/', options)
      .map(response => {
        const users = response.json();
        return users.map((user) => new User(user));
      })
      .catch(this.handleError);
  }

  // API: GET /api/users/roles/:role
  public getUsersInRole(role: string): Observable<User[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/users/roles/' + role.toLowerCase(), options)
      .map(response => {
        const users = response.json();
        return users.map((user) => new User(user));
      })
      .catch(this.handleError);
  }

  // API: PATCH /api/users/
  public updateUserById(user: User): Observable<User> {
    const options = this.getRequestOptions();
    return this.http
      .patch(API_URL + '/api/users/' + user.applicationUserId, user, options)
      .map(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // API: GET /api/users/{userId}/roles/add/{roleId}
  public addToRole(userId: string,  role: string): Observable<User>  {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/users/' + userId + '/roles/add/' + role.toLowerCase(), options)
      .map(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // API: GET /api/users/{userId}/roles/remove/{roleId}
  public removeToRole(userId: string,  role: string): Observable<User>  {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/users/' + userId + '/roles/remove/' + role.toLowerCase(), options)
      .map(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // API: DELETE /api/users/
  public deleteUser(user: User) {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/api/users/' + user.applicationUserId, options)
      .map((_) => {})
      .catch(this.handleError);
  }

  // API: POST /security/token
  public login(email: string, password: string) {
    return this.http
      .post(API_URL + '/api/security/token', {
        'email': email,
        'password': password
      })
      .map(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  // API: POST /account/register
  public addUser(user: RegisterUser): Observable<RegisterUser> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/api/account/register/', user, options)
      .map(response => {})
      .catch(this.handleError);
  }

  // API: GET /api/frequencytypes
  public getAllFrequencyTypes(): Observable<FrequencyType[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/api/frequencytypes', options)
      .map(response => {
        const frequencytypes = response.json();
        return frequencytypes.map((ft) => new FrequencyType(ft));
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

  private getRequestOptions() {
    const headers = new Headers({
      'Authorization': 'Bearer ' + this.session.accessToken
    });
    return new RequestOptions({ headers });
  }
}
