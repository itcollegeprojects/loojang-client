import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ApiService } from './api.service';

@Injectable()
export class AuthService {

  constructor(
    private session: SessionService,
    private api: ApiService
  ) {
  }

  public isSignedIn() {
    if (this.session.accessToken == null) {
      this.loadSession();
    }
    return !!this.session.accessToken;
  }

  public doSignOut() {
    this.session.destroy();
    this.removeSession();
  }

  public doSignIn(accessToken: string, roles: string[], name: string, id: string) {
    if (!accessToken) {
      return;
    }
    this.session.accessToken = accessToken;
    this.session.roles = roles;
    this.session.name = name;
    this.session.id = id;
    this.session.currentRole = roles.length > 0 ? roles[0] : 'roleless';
    this.saveSession();
  }

  public saveSession(): void {
    sessionStorage.setItem('usersession', JSON.stringify(this.session));
  }

  public removeSession(): void {
    sessionStorage.setItem('usersession', JSON.stringify(null));
  }

  public loadSession(): void {
    const session = JSON.parse(sessionStorage.getItem('usersession'));
    try {
      this.session.loadSession(session);
    } catch (error) {

    }
  }

  public getUserInfo() {
    return {
      id: this.session.id,
      name: this.session.name,
      roles: this.session.name ? this.session.roles : []
    };
  }

  get userRoles(): Array<string> {
    return this.session.name ? this.session.roles : [];
  }

  get currentRole(): string {
    return this.session.currentRole;
  }

  set currentRole(role: string) {
    if (this.hasRole(role)) { this.session.currentRole = role; }
  }

  public hasRole(role: string) {
    if (this.session.roles == null) { return false; }
    return this.session.roles.indexOf(role.toUpperCase()) > -1;
  }
}
