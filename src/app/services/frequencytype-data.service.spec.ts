import { TestBed, inject } from '@angular/core/testing';

import { FrequencytypeDataService } from './frequencytype-data.service';

describe('FrequencytypeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FrequencytypeDataService]
    });
  });

  it('should be created', inject([FrequencytypeDataService], (service: FrequencytypeDataService) => {
    expect(service).toBeTruthy();
  }));
});
