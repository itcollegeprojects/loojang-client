import { Injectable } from '@angular/core';
import { FrequencyType } from '../models/frequencytype.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FrequencyTypeDataService {

  constructor(
    private api: ApiService
  ) {
  }

  getAllFrequencyTypes(): Observable<FrequencyType[]> {
    return this.api.getAllFrequencyTypes();
  }
}
