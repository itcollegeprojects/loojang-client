import { Injectable } from '@angular/core';
import { MedicalCase } from '../models/medicalcase.model';
import { Prescription } from '../models/prescription.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MedicalCaseDataService {

  constructor(
    private api: ApiService
  ) {
  }

  getAllMedicalCases(): Observable<MedicalCase[]> {
    return this.api.getAllMedicalCases();
  }

  getMedicalCasesByPatientId(patientId: number): Observable<MedicalCase[]> {
    return this.api.getMedicalCasesByPatientId(patientId);
  }

  getMedicalCasesByDoctorId(doctorId: string): Observable<MedicalCase[]> {
    return this.api.getMedicalCasesByDoctorId(doctorId);
  }

  getMedicalCaseById(medicalCaseId: number): Observable<MedicalCase> {
    return this.api.getMedicalCaseById(medicalCaseId);
  }

  addMedicalCase(medicalCase: Partial<MedicalCase>): Observable<MedicalCase> {
    return this.api.addMedicalCase(medicalCase);
  }

  deleteMedicalCaseById(medicalCaseId: number): Observable<number> {
    return this.api.deleteMedicalCaseById(medicalCaseId);
  }

  updateMedicalCase(medicalCase: MedicalCase): Observable<MedicalCase> {
    for (const p of medicalCase.prescriptions) {
      this.api.addPrescription(p)
        .subscribe( (_) => null);
    }
    medicalCase.prescriptions = [];
    return this.api.updateMedicalCase(medicalCase);
  }
}
