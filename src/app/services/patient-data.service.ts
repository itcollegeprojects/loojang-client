import { Injectable } from '@angular/core';
import { Patient } from '../models/patient.model';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PatientDataService {

  constructor(
    private api: ApiService,
    private auth: AuthService
  ) {
  }

  deletePatientById(patientId: number): Observable<number> {
    return this.api.deletePatientById(patientId);
  }

  getAllPatients(): Observable<Patient[]> {
    return this.api.getAllPatients();
  }

  getPatientsByRole(): Observable<Patient[]> {
    return this.api.getPatientsByRole(this.auth.currentRole);
  }

  getPatientById(patientId: number): Observable<Patient> {
    return this.api.getPatientById(patientId);
  }

  addPatient(patient: Patient): Observable<Patient> {
    return this.api.addPatient(patient);
  }

  updatePatient(patient: Patient): Observable<Patient> {
    return this.api.updatePatient(patient);
  }
}
