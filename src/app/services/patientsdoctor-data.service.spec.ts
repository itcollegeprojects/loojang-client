import { TestBed, inject } from '@angular/core/testing';

import { PatientsDoctorDataService } from './patientsdoctor-data.service';

describe('PatientsDoctorDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatientsDoctorDataService]
    });
  });

  it('should be created', inject([PatientsDoctorDataService], (service: PatientsDoctorDataService) => {
    expect(service).toBeTruthy();
  }));
});
