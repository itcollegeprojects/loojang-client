import { Injectable } from '@angular/core';
import { PatientsDoctor } from '../models/patientsdoctor.model';
import { PatientToDoctor } from '../models/patienttodoctor.model';
import { User } from '../models/user.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PatientsDoctorDataService {

  constructor(
    private api: ApiService
  ) {
  }

  addPatientToDoctor(patientToDoctor: PatientToDoctor): Observable<PatientToDoctor> {
    return this.api.addPatientsDoctor(patientToDoctor);
  }

  getPatientsDoctorsByPatientId(patientId: number): Observable<PatientsDoctor[]> {
    return this.api.getPatientsDoctorsByPatientId(patientId);
  }

  getAvailableDoctors(): Observable<User[]> {
    return this.api.getUsersInRole('DOCTOR');
  }

  updatePatientFromDoctor(patientsDoctor: PatientsDoctor): Observable<PatientsDoctor> {
    return this.api.updatePatientsDoctor(patientsDoctor);
  }

  removePatientFromDoctor(patientsDoctor: PatientsDoctor): Observable<PatientsDoctor> {
    return this.api.deletePatientsDoctorById(patientsDoctor.patientsDoctorId);
  }

  getPatientsDoctorsByDoctorsAndPatientsIds(doctorId: string, patientId: number) {
    return this.api.getPatientsDoctorsByPatientId(patientId)
                  .map(data => data.filter(p => p.applicationUserId === doctorId))
                  .map(data => {
                    data.sort((a, b) => {
                        return a.toDate > b.toDate ? -1 : 1;
                     });
                    return data[0];
                   });
  }
}
