import { TestBed, inject } from '@angular/core/testing';

import { PatientsguardianDataService } from './patientsguardian-data.service';

describe('PatientsguardianDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatientsguardianDataService]
    });
  });

  it('should be created', inject([PatientsguardianDataService], (service: PatientsguardianDataService) => {
    expect(service).toBeTruthy();
  }));
});
