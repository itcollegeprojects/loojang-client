import { Injectable } from '@angular/core';
import { PatientsGuardian } from '../models/patientsguardian.model';
import { PatientToGuardian } from '../models/patienttoguardian.model';
import { User } from '../models/user.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PatientsGuardianDataService {

  constructor(
    private api: ApiService
  ) {
  }

  addPatientToGuardian(patientToGuardian: PatientToGuardian): Observable<PatientToGuardian> {
    return this.api.addPatientsGuardian(patientToGuardian);
  }

  getPatientsGuardiansByPatientId(patientId: number): Observable<PatientsGuardian[]> {
    return this.api.getPatientsGuardiansByPatientId(patientId);
  }

  getAvailableGuardians(): Observable<User[]> {
    return this.api.getUsersInRole('DOCTOR');
  }

  updatePatientFromGuardian(patientsGuardian: PatientsGuardian): Observable<PatientsGuardian> {
    return this.api.updatePatientsGuardian(patientsGuardian);
  }

  removePatientFromGuardian(patientsGuardian: PatientsGuardian): Observable<PatientsGuardian> {
    return this.api.deletePatientsGuardianById(patientsGuardian.patientsGuardianId);
  }

  getPatientsGuardiansByGuardiansAndPatientsIds(guardianId: string, patientId: number) {
    return this.api.getPatientsGuardiansByPatientId(patientId)
                  .map(data => data.filter(p => p.applicationUserId === guardianId))
                  .map(data => {
                    data.sort((a, b) => {
                        return a.toDate > b.toDate ? -1 : 1;
                     });
                    return data[0];
                   });
  }
}
