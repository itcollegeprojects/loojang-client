import { TestBed, inject } from '@angular/core/testing';

import { PatientsRoomDataService } from './patientsroom-data.service';

describe('PatientsRoomDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatientsRoomDataService]
    });
  });

  it('should be created', inject([PatientsRoomDataService], (service: PatientsRoomDataService) => {
    expect(service).toBeTruthy();
  }));
});
