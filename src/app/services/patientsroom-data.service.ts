import { Injectable } from '@angular/core';
import { PatientsRoom } from '../models/patientsroom.model';
import { PatientToRoom } from '../models/patienttoroom.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PatientsRoomDataService {

  constructor(
    private api: ApiService
  ) {
  }

  addPatientToRoom(patientToRoom: PatientToRoom): Observable<PatientToRoom> {
    return this.api.addPatientToRoom(patientToRoom);
  }

  getPatientsRoomsByPatientId(patientId: number): Observable<PatientsRoom[]> {
    return this.api.getPatientsRoomsByPatientId(patientId);
  }

}
