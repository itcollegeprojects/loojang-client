import { TestBed, inject } from '@angular/core/testing';

import { PrescriptionDataService } from './prescription-data.service';

describe('PrescriptionDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrescriptionDataService]
    });
  });

  it('should be created', inject([PrescriptionDataService], (service: PrescriptionDataService) => {
    expect(service).toBeTruthy();
  }));
});
