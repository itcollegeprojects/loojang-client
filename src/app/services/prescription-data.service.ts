import { Injectable } from '@angular/core';
import { Prescription, PrescriptionDescription } from '../models/prescription.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PrescriptionDataService {

  constructor(
    private api: ApiService
  ) {
  }

  getPrescriptionsByMedicalCaseId(medicalCaseId: number): Observable<PrescriptionDescription[]> {
    return this.api.getPrescriptionsByMedicalCaseId(medicalCaseId);
  }

  addPrescription(prescription: Prescription): Observable<Prescription> {
    return this.api.addPrescription(prescription);
  }

  deletePrescriptionById(prescriptionId: number): Observable<number> {
    return this.api.deletePrescriptionById(prescriptionId);
  }

  updatePrescription(prescription: Prescription): Observable<Prescription> {
    return this.api.updatePrescription(prescription);
  }
}
