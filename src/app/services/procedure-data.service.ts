import { Injectable } from '@angular/core';
import { Procedure } from '../models/procedure.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProcedureDataService {

  constructor(
    private api: ApiService
  ) {
  }

  deleteProcedureById(procedureId: number): Observable<number> {
    return this.api.deleteProcedureById(procedureId);
  }

  deleteProcedure(procedure: Procedure): Observable<number> {
    return this.api.deleteProcedureById(procedure.procedureId);
  }

  getAllProcedures(): Observable<Procedure[]> {
    return this.api.getAllProcedures();
  }

  getActiveProcedures(): Observable<Procedure[]> {
    return this.api.getActiveProcedures();
  }

  getProceduresBetweenDates(dateFrom: Date, dateTo: Date): Observable<Procedure[]> {
    return this.api.getProceduresBetweenDates(dateFrom, dateTo);
  }

  getProcedureById(ProcedureId: number): Observable<Procedure> {
    return this.api.getProcedureById(ProcedureId);
  }

  addProcedure(procedure: Procedure): Observable<Procedure> {
    return this.api.addProcedure(procedure);
  }

  updateProcedure(procedure: Procedure): Observable<Procedure> {
    return this.api.updateProcedure(procedure);
  }

  toggleProcedureComplete(procedure: Procedure): Observable<Procedure> {
    if (!procedure.isCompleted()) {
      procedure.actualDate = new Date();
    } else {
      procedure.actualDate = null;
    }
    try {
      return this.api.updateProcedure(procedure);
    } catch(e) {
      procedure.actualDate = null;
      return this.api.updateProcedure(procedure);
    }
  }

}
