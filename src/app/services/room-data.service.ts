import { Injectable } from '@angular/core';
import { Room } from '../models/room.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RoomDataService {

  constructor(
    private api: ApiService
  ) {
  }

  deleteRoomById(roomId: number): Observable<number> {
    return this.api.deleteRoomById(roomId);
  }

  getAllRooms(): Observable<Room[]> {
    return this.api.getAllRooms();
  }

  getFreeRooms(): Observable<Room[]> {
    return this.api.getFreeRooms();
  }

  getRoomById(roomId: number): Observable<Room> {
    return this.api.getRoomById(roomId);
  }

  addRoom(room: Room): Observable<Room> {
    return this.api.addRoom(room);
  }

  updateRoom(room: Room): Observable<Room> {
    return this.api.updateRoom(room);
  }
}
