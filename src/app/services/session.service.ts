import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  public accessToken: string;
  public roles: string[];
  public currentRole = 'roleless';
  public name: string;
  public id: string;

  constructor() {
  }

  public destroy(): void {
    this.accessToken = null;
    this.roles = null;
    this.currentRole = 'roleless';
    this.name = null;
    this.id = null;
  }

  public loadSession(session): void {
    this.accessToken = session.accessToken;
    this.roles = session.roles;
    this.currentRole = session.currentRole;
    this.name = session.name;
    this.id = session.id;
  }
}


