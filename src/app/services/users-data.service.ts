import { Injectable } from '@angular/core';
import { PatientsDoctor } from '../models/patientsdoctor.model';
import { User } from '../models/user.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsersDataService {

  constructor(
    private api: ApiService
  ) {
  }

  getUsersInRole(role: String): Observable<User[]> {
    return this.api.getUsersInRole(role.toUpperCase());
  }

  getAllUsers(): Observable<User[]> {
    return this.api.getAllUsers();
  }

  updateUser(user: User): Observable<User> {
    return this.api.updateUserById(user);
  }

  deleteUser(user: User): Observable<User> {
    return this.api.deleteUser(user);
  }

  addToRole(userId: string, role: string): Observable<User> {
    return this.api.addToRole(userId, role);
  }

}
